import csv
import requests
import json
import os
import datetime
import time
import sys
import glob
import thread
import threading
from threading import Thread
import os.path
import traceback
import re
import collections
import calendar
import itertools as it
import httplib2
from lxml import etree, html
from selenium import webdriver
from selenium.webdriver import ChromeOptions 
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders
import math
from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
import sqlalchemy
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String, DateTime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session
from sqlalchemy.ext.declarative import declarative_base
from BeautifulSoup import BeautifulSoup
import sqlite3
import shutil
from dateutil import tz
from string import digits




# Test Switches:
debug= 0
useReq = 1
quarter	= '4_2018'


try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None
GETDBURL = 'http://fakeurl.com/api'
SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Google Sheets API Python Quickstart'

dir = os.path.dirname(__file__)
if not dir:
	dir = os.getcwd()
updateScript = dir + '/makeTotalTable.py'
dlFolder = dir + '\\_DL'
dataBasePath_local= dir + '/techTracker.db'
dataBaseTotalsPath_local= dir + '/techTracker_totals.db'
dataBaseJsonPath_local= dir + '/data'
dataBaseJsonPath_cloud= 'G:/My Drive/DBs/techTracker/data'
dataBasePath_cloud= 'G:/My Drive/DBs/techTracker/'
if not os.path.exists(dlFolder):
	print "DL folder does not exist"
	os.makedirs(dlFolder)
else:
	print "DL folder exists"
print dir
print dlFolder
screenlock = threading.Semaphore(1)
dblock = threading.Semaphore(1)
sem = threading.Semaphore(10)
csvSem = threading.Semaphore(1)
dstDiff = 8 - time.localtime().tm_isdst
techHist = os.path.join(dir,'testData4.csv')
techDpfvHistory = 'https://mes.com/web#screen=exc&time=1547232808589&id=2978'
mesHistoryURL = 'http://production.com/mes-api/p23439/units/%s/history'
pitaDutSearchURL = 'http://at2.ssj.mfg.networks.com/dmfg/mfg/search/'
pitaSearchRunUrl = 'http://at2.ssj.mfg.networks.com/dmfg/mfg/search/run/' # POST 
server = 'http://at2.ssj.mfg.networks.com'
updateExistingThreadList = []
username = "shopfloor"
password = "S********"
dpfvHistory = dlFolder + '/1_DPFV_Hist_Q2_18.csv'

connectErrorList = []

touchDict = collections.defaultdict(list)		
list_of_tests = ['1 TEST BURN IN',
				'Ar FVT',
				'Ar ESS',
				'1 TEST HI POT',
				'Ar FST',
				'Ar Mech Station 3',
				'1 INSP IN PROCESS QC 1',
				'1 INSP FQC',
]
	
testOrderdict={ 
				'Ar Mech Station 3':4,
				'1 INSP IN PROCESS QC 1':5,
				'Ar FVT':6,
				'Ar ESS':7,
				'1 TEST BURN IN':8,
				'Ar FST':9,
				'1 INSP FQC':10,
				}

testTranslator={'AriArsta FVT':['fvt-interactive','fvt-configure','fvt'],
				'1 TEST BURN IN':['burnin'],
				'Ar ESS':['ess'],
				'Ar FST':['fst-interactive','fst'],
				'1 TEST ORT 1':['ort']}

xpaths = { 'searchTxtBox' : '//*[@id="sn"]',
			'usernameTxtBox' : '//*[@id="username"]',
			'passwordTxtBox' : '//*[@id="password"]',
			'loginBtn' : '//*[@id="Submit"]',
			's1NextBtn' : '//*[@id="container"]/div[1]/div/div/div[2]/div[3]/div[1]/div[2]/button[1]/span',
			'serialsTextBox' : "//*[@id='container']/div[1]/div/div/div[2]/div[3]/div[2]/div[1]/div[1]/div[1]/div/div/textarea",
			's2FromDate_input' : "//*[@id='container']/div[1]/div/div/div[2]/div[3]/div[2]/div[1]/div[2]/div[1]/div/div/div[1]/input",
			's2ToDate_input' : "//*[@id='container']/div[1]/div/div/div[2]/div[3]/div[2]/div[1]/div[2]/div[1]/div/div/div[2]/input",
			'gobtn' : "//*[@id='container']/div[1]/div/div/div[2]/div[3]/div[2]/div[2]/button[2]",
			'xlsxbtn' : "/html/body/div[3]/div/ul/li[2]/a",
			'xlsxbtn_old' : "/html/body/div[10]/div/ul/li[4]",
			'csvbtn' : "/html/body/div[3]/div/ul/li[4]/a",
			'dropdownbtn' : "//*[@id='container']/div[1]/div/div/div[2]/div[2]/div/div/div[1]/span[1]/button[2]/span[1]",
			'sosTextBox' : "//*[@id='container']/div[1]/div/div/div[2]/div[3]/div[1]/div[1]/div[2]/div[1]/div/div/textarea"}

serials = ['633872\n',
			'634437\n',
			'7885\n',
			'632861\n',
			'633993\n',
			'634663\n',
			'634705\n',
			'635157\n',
			'622377\n',
			'636107\n',
			'636479\n',
			'634788\n',
			'628696\n',
			'635083\n',
			'146360\n',
			'633956\n',
			'636874\n',
			'637183\n',
			'627542']

quarters_dict = {'January':1,
				'February':1,
				'March':1,
				'April':2,
				'May':2,
				'June':2,
				'July':3,
				'August':3,
				'September':3,
				'October':4,
				'November':4,
				'December':4}
				
quarter_num = {1:['01','03'],
				2:['04','06'],
				3:['07','09'],
				4:['10','12']}

dataReadSearch = '(?:========== BEGIN: Data read before [A-Za-z]* ==========\n)(.*)'#\n)'
testFailSearch = '(##! TEST FAIL: .*)'
cmdFailSearch = '(##! COMMAND FAIL: .*)'
pyException = "(==================== Exception raised in '.*====================)"
fatalError = 'FATAL ERROR:.*'#\n'
devSearch = '\n.*!!! DEV-[0-9]* applied to wrong PCA \([A-Za-z0-9]*\) !!!'
testSetError = 'TEST SETUP ERROR: .*\n'
fvtIerror= 'FAIL:.*'
burntemp = 'burnin station temperature is expected to be at .*'

def addColumn():
	dbCon = sqlite3.connect("techTracker.db")
	cur   = dbCon.cursor()
	addColumn = "ALTER TABLE trackerData ADD COLUMN orgFailNoNum String(1000)"
	cur.execute(addColumn)
	dbCon.close()

def convertTime(unConvTime):
	utcOffset = int(unConvTime[-3:]) 
	convTime = datetime.datetime.strptime(dateunConvTime[:-3],"%Y-%m-%d %H:%M:%S")	#Initial fail time
	convTime = convTime - datetime.timedelta(hours=utcOffset)

## CREATE THE DATABASE
engine = create_engine('sqlite:///techTracker.db')
Base = declarative_base(bind=engine)

class Unit(Base):
	__tablename__ 	= 'trackerData'
	id 				= Column(Integer, primary_key=True)
	serial  		= Column(String(40),nullable=False)
	mesTest 		= Column(String(40),nullable=False)
	pitaTest		= Column(String(40),nullable=False)
	failKey			= Column(String(10),nullable=True)
	failTime		= Column(String(30),nullable=True)
	failText 		= Column(String(1000),nullable=True)
	user			= Column(String(20), nullable=False)
	dpfvKey			= Column(String(10),nullable=False)
	dpfvTime		= Column(String(30), nullable=False)
	partNumber		= Column(String(60), nullable=False)
	fix				= Column(String(60),nullable=True)
	bgarKey			= Column(String(10),nullable=True)
	bgarTime 		= Column(String(30), nullable=True)
	dgqcKey			= Column(String(10),nullable=True)
	dgqcTime 		= Column(String(30), nullable=True)
	passFailKey		= Column(String(10),nullable=True)
	passFailTime 	= Column(String(30), nullable=True)
	postFailMesTest	= Column(String(40),nullable=False)
	postFailPitaTest= Column(String(40),nullable=True)
	postFailText	= Column(String(1000),nullable=True)
	resultType	 	= Column(String(5), nullable=True)
	comment 		= Column(String(100), nullable=True)
	deviations		= Column(String(30), nullable=True)
	week			= Column(String(10), nullable=False)
	quarter			= Column(String(10), nullable=True)
	orgFailNoNum	= Column(String(1000),nullable=True)
	month			= Column(String(10), nullable=True)
	def __init__(self, serial, mesTest, pitaTest, failKey, failTime, failText, user, dpfvKey, dpfvTime, partNumber, postFailMesTest, week, fix=None,
					bgarKey=None, bgarTime=None, dgqcKey=None, dgqcTime=None, passFailKey=None, 
					passFailTime=None,postFailPitaTest=None ,postFailText=None, resultType=None, comment=None, deviations = None, quarter=None, orgFailNoNum=None,
					month=None):
		self.serial 		= serial
		self.mesTest 		= mesTest
		self.pitaTest 		= pitaTest
		self.failKey		= failKey
		self.failTime		= failTime
		self.failText 		= failText
		self.fix 			= fix
		self.user 			= user
		self.dpfvKey 		= dpfvKey
		self.dpfvTime 		= dpfvTime
		self.partNumber		= partNumber
		self.postFailMesTest= postFailMesTest
		self.week			= week
		self.bgarKey 		= bgarKey
		self.bgarTime 		= bgarTime
		self.dgqcKey 		= dgqcKey
		self.dgqcTime 		= dgqcTime
		self.passFailKey	= passFailKey
		self.passFailTime 	= passFailTime
		self.postFailPitaTest=	postFailPitaTest
		self.postFailText	= postFailText
		self.resultType		= resultType
		self.comment 		= comment
		self.deviations		= deviations
		self.quarter		= quarter
		self.orgFailNoNum	= orgFailNoNum
		self.month			= month

class Tech(Base):
	__tablename__ 	= 'techData'
	id 				= Column(Integer, primary_key=True)
	name  			= Column(String(40),nullable=False)
	empID 			= Column(String(40),nullable=False)
	dateAdded		= Column(String(30), default=datetime.datetime.utcnow)
	def __init__(self,name,empID):
		self.name	= name
		self.empID	= empID
	
Base.metadata.create_all()
dbSession = sessionmaker(bind=engine)
dbs =scoped_session(dbSession)

def debugPrint(text):
	if debug == 1:
		print text

def days_hours_minutes(td):
    return td.days, td.seconds//3600, (td.seconds//60)%60

def translateTest(pitaTest,serial):	# Translate PITA test into MES Test ('burnin'->'1 TEST BURN IN')
	for item in testTranslator:
		if pitaTest in testTranslator[item]:
			failedMESTest = item
			return failedMESTest
	print 'Could not translate PITA test: %s into MES test for SN: %s' % (pitaTest,serial)

# Credentials for Google API
def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'sheets.googleapis.com-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print 'Storing credentials to ' + credential_path
    return credentials

def getDebugEmployees():
	
	credentials = get_credentials()
	http = credentials.authorize(httplib2.Http())
	discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
					'version=v4')
	service = discovery.build('sheets', 'v4', http=http,
								discoveryServiceUrl=discoveryUrl)

	spreadsheetId = '1lA2JsbrA0itWHpZret1R'
	rangeName = 'Sheet1!A2:B'
	result = service.spreadsheets().values().get(
		spreadsheetId=spreadsheetId, range=rangeName).execute()
	values = result.get('values', [])
	empNums = []
	empList = []
	if not values:
		print 'No data found.'
	else:
		for row in values:
			if len(row) == 1:
				shift = row[0] 
			else:
				empNums.append(row[1] + "\n")
				empList.append(row[1])
				if not len(dbs.query(Tech).filter(Tech.empID == row[1], Tech.name == row[0]).all()) > 0:
					tech = Tech(row[0],row[1])
					dblock.acquire()
					dbs.add(tech)
					dbs.commit()
					dblock.release()
				print '%s, %s' % (row[0], row[1])
		data = dbs.query(Tech).all()
	
	return empNums, empList

def getQuarterDates():
	qnum = int(quarter.split('_')[0])
	qYear	= int(quarter.split('_')[1])
	qToGet = quarter_num[qnum]
	startMonth = int(qToGet[0])
	endMonth	= int(qToGet[1])
	startDate = datetime.date(qYear,startMonth,01)
	daysInMonth = calendar.monthrange(qYear,endMonth)[1]
	endDate = datetime.date(qYear,endMonth,daysInMonth)
	return startDate,endDate
	
def getWeekDates():
	today = datetime.date.today()
	endDate = today - datetime.timedelta(days=2)	# Assume script is run on monday, this finds the date of the previous saturday
	startDate = today - datetime.timedelta(days=8)	# Assume script is run on monday, this finds the date of two Sundays ago
	print "StartDate: %s" %str(startDate)
	print "EndDate: %s" %str(endDate)
	return startDate,endDate
	
def tempdates():
	today = datetime.date.today()
	endDate = today - datetime.timedelta(days=4)
	print "StartDate: %s" %str(endDate)
	print "EndDate: %s" %str(today)
	return endDate,today
	
def getTechHist(empNumbers):#techNum,startDate,endDate):
	try:
		# We are using Chrome
		chromedriver_path = "C:/Python27/chromedriver_2.35.exe"
		chrome_options = Options() 
		chrome_options.add_argument("--start-maximized")
		chrome_options.add_argument("--log-level=3")
		# Set path for downloaded files
		prefs = {"download.default_directory" : dlFolder}
		chrome_options.add_experimental_option("prefs",prefs)
		chrome_options.binary_location = "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe"
		mydriver = webdriver.Chrome(executable_path=chromedriver_path, chrome_options=chrome_options)
		# Open the URL
		mydriver.get(techDpfvHistory)
		# Tells the program to wait for 10 seconds before throwing an exception
		mydriver.implicitly_wait(10)
		# Used for explicit waits, program will wait for 30 seconds before throwing exception, while continuously looking for an element
		wait = WebDriverWait(mydriver, 900)
		# Clear Username TextBox if already allowed "Remember Me" 
		mydriver.find_element_by_xpath(xpaths['usernameTxtBox']).clear()
		# Write Username in Username TextBox
		mydriver.find_element_by_xpath(xpaths['usernameTxtBox']).send_keys(username)
		# Clear Password TextBox if already allowed "Remember Me" 
		mydriver.find_element_by_xpath(xpaths['passwordTxtBox']).clear()
		# Write Password in password TextBox
		mydriver.find_element_by_xpath(xpaths['passwordTxtBox']).send_keys(password)
		# Click Login button
		mydriver.find_element_by_xpath(xpaths['loginBtn']).click()
		
		wait.until(EC.invisibility_of_element_located((By.CLASS_NAME, "loader")))
		
		
		# Wait until the dropdown menu is clickable or for 30 seconds	
		dropdownelement = wait.until(EC.element_to_be_clickable((By.XPATH, "//*[@id='container']/div[1]/div/div/div[2]/div[3]/div/div[1]/div[1]/div[1]/div/div/select/option[text() = 'MESR_e26']")))
		# # Click the dropdown menu
		dropdownelement.click()

		mydriver.find_element_by_xpath(xpaths['s1NextBtn']).click()
		time.sleep(1)
		
		# FIND Employee number TESTAREA
		element= wait.until(EC.presence_of_element_located((By.XPATH, "//*[@id='container']/div[1]/div/div/div[2]/div[3]/div[2]/div[1]/div[1]/div[1]/div/div/textarea")))
		# Place the employee numbers into the search box
		mydriver.find_element_by_xpath(xpaths['serialsTextBox']).send_keys(empNumbers)
		
		startDate,endDate	= getWeekDates()
		startDate_str 		= startDate.strftime("%m/%d/%Y")
		endDate_str			= endDate.strftime("%m/%d/%Y")
		# Add From Date
		mydriver.find_element_by_xpath(xpaths['s2FromDate_input']).clear()
		# Write PN in textbox
		mydriver.find_element_by_xpath(xpaths['s2FromDate_input']).send_keys(startDate_str)
		
		# Add To Date
		mydriver.find_element_by_xpath(xpaths['s2ToDate_input']).clear()
		# Write PN in textbox
		mydriver.find_element_by_xpath(xpaths['s2ToDate_input']).send_keys(endDate_str)
		
		mydriver.find_element_by_xpath(xpaths['serialsTextBox']).click()

		time.sleep(1)
		
		mydriver.find_element_by_xpath(xpaths['gobtn']).click()
		print 'DL: %s' % dlFolder
		dropdownelement = wait.until(EC.element_to_be_clickable((By.XPATH, "//*[@id='container']/div[1]/div/div/div[2]/div[2]/div/div/div[1]/span[1]/button[2]/span[1]")))
		# # Click the dropdown menu
		dropdownelement.click()

		# Click on "CSV" within the dropdown menu
		mydriver.find_element_by_xpath(xpaths['csvbtn']).click()
		# wait for 1 second. This may not be needed, but gives time for the file to download
		time.sleep(5)
		while not glob.glob(dlFolder + '/Tech_DPFV_History*.csv'):
			print 'Still Downloading'
			time.sleep(5)
		# Close the window
		mydriver.quit()
		# Move file and rename
		if os.path.exists(dpfvHistory):
			os.remove(dpfvHistory)
		file = max(glob.iglob( dlFolder + '/Tech_DPFV_History*.csv'), key=os.path.getctime)
		
		os.rename(file, dpfvHistory )
	except:
		# Get the traceback object
 		tb = sys.exc_info()[2]
 		tbinfo = traceback.format_tb(tb)[0]
 		# Concatenate information together concerning the error into a message string
		pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
		print pymsg
 		# Close the Browser Window
 		mydriver.quit()


def parsetechHist(file):
	with open(file, 'rb') as f:
		mycsv = csv.reader(x.replace('\0', '') for x in f) # This fixes the 'Null byte' issue
		mycsv = list(mycsv)
	
	for row in mycsv[1:]:
		# print row
		partNum	= row[3]
		action	= row[5]
		location= row[6]
		if ("LFARISASY" in partNum) and (action in ["11","10"]) and (location in ["DPFV "]):	# Tech touched a board
			empNum	= row[0]
			date	= row[1]
			serial	= row[2]
			act_key	= row[4]
			if not touchDict[empNum]:
				touchDict[empNum] = collections.defaultdict(list)
			touchDict[empNum][serial].append([act_key,date,partNum])
			
def getFix(unit,repairedDefKeys):
	board_loc_List=[]
	fix = ''
	if unit['data']['defects']:
		for defect_item in unit['data']['defects']:
			if defect_item['repair_activity_key'] in repairedDefKeys: 
				if (defect_item['board_loc'] not in ['None',None,'']):
					board_loc_List.append(defect_item['board_loc'])
				else:
					board_loc_List.append('Retest')
	if board_loc_List:
		if (len(list(set(board_loc_List))) == 1) and (list(set(board_loc_List))[0] == 'Retest'):
			fix = 'Retest'
		else:
			board_loc_List=filter(lambda a: a != 'Retest', board_loc_List)							# Remove all occurances of 'Retest' 
			fix = ','.join(board_loc_List)
	else:
		fix= 'Retest'
	return fix
	
def getFailText(serial,operator,date,test,flowErr,endTime):
	noFind = 'No Failures,Invalid or ubg found in PITA'
	if flowErr:
		debugPrint("FLOW ERRORS FOUND!")
	#HEADLESS!
	debugPrint("endTime: %s" %endTime)
	find = ''
	testName = ''
	mesTest = test
	chromedriver_path = "C:/Python27/chromedriver_2.43.exe"
	chrome_options = Options() 
	chrome_options.add_argument("--headless")
	chrome_options.add_argument("--disable-extensions")
	chrome_options.add_argument("--disable-gpu")
	chrome_options.add_argument("--log-level=3")
	chrome_options.binary_location = "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe"
	mydriver = webdriver.Chrome(executable_path=chromedriver_path, chrome_options=chrome_options)
	
	# Open the URL
	mydriver.get(pitaDutSearchURL)
	# Tells the program to wait for 10 seconds before throwing an exception
	mydriver.implicitly_wait(10)
	# Used for explicit waits, program will wait for 30 seconds before throwing exception, while continuously looking for an element
	wait = WebDriverWait(mydriver, 60)
	# Make sure table has loaded
	searchBtn = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/form[1]/button')))
	time.sleep(1)
	mydriver.find_element_by_xpath(xpaths['searchTxtBox']).send_keys(serial)
	searchBtn.click()
	wait.until(EC.presence_of_element_located((By.XPATH,'/html/body/div/table/tbody/tr[2]')))
	histBtn = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/table/tbody/tr[2]/td[4]/a')))
	histBtn.click()
	# Send html over to lxml
	html_source = mydriver.page_source
	tree = html.fromstring(html_source)
	# Number of rows  in the test/notes section
	numOfElements = len(tree.xpath('/html/body/div[2]/div[1]/table/tbody/tr'))
	# Put each row into list as an element
	elementArray = tree.xpath('/html/body/div[2]/div[1]/table/tbody/tr')
	utcOffset = int(endTime[-3:]) #dstDiff + int(endTime[-3:])
	endTime_datetime = datetime.datetime.strptime(endTime[:-3],"%Y-%m-%d %H:%M:%S")	#DPFV time
	endTime_datetime = endTime_datetime - datetime.timedelta(hours=utcOffset)
	
	bodyText=tree.xpath('/html/body/text()')

	for idx, item in enumerate(elementArray):
		# Grab the test name and result elements (column 1 & 2)
		tname = tree.xpath('/html/body/div[2]/div[1]/table/tbody/tr[%s]/td[1]' % (idx+1))
		result = tree.xpath('/html/body/div[2]/div[1]/table/tbody/tr[%s]/td[2]' % (idx+1))
		# Store the text of the test name and result elements
		testName = tname[0].text_content()
		testResult = result[0].text_content()
		
		# If we have reached the last FAIL, lets check it out
		if testResult in ['failed','invalid','bug']:
			if (testName in ['edvt']) or (translateTest(testName,serial) == test) or (flowErr):
				debugPrint("Test fail match! MES: %s | PITA: %s" %(test,testName))
				## Grab Operator Name
				operator_name = tree.xpath('/html/body/div[2]/div[1]/table/tbody/tr[%s]/td[4]/text()' % (idx+1))[0]
				debugPrint("Operators! MES: %s | PITA: %s" %(operator,operator_name))
				
				utcOffset = int(date[-3:]) #dstDiff + int(date[-3:])
				date_datetime = datetime.datetime.strptime(date[:-3],"%Y-%m-%d %H:%M:%S")	#Initial fail time
				date_datetime = date_datetime - datetime.timedelta(hours=utcOffset)

				pitaDate = mydriver.find_element_by_xpath('/html/body/div[2]/div[1]/table/tbody/tr[%s]/td[2]/span' % (idx+1))
				pitaDate = pitaDate.get_attribute('title')
				
				testduration = pitaDate.split()[-1].strip("()")
				testduration = testduration.split(":")
				testStartTime = pitaDate.split(' (')[0]
				startTime = datetime.datetime.strptime(testStartTime, "%Y-%m-%d %H:%M:%S")
				debugPrint("MES Time: %s | PITA Time: %s" %(str(date_datetime), str(startTime)))
				if (testResult in ['invalid']) or (endTime_datetime > startTime > date_datetime) :
					if date_datetime < startTime < endTime_datetime:
						timeDiff = 1
					else:
						timeDiff = -1
				
				else:
					endTime = startTime + datetime.timedelta(hours=int(testduration[0]),minutes=int(testduration[1]),seconds=int(testduration[2]))
					timeDiff = (date_datetime - endTime).total_seconds() / 60.0
				debugPrint("Heres the timediff: %d" %timeDiff)
				if 15 > timeDiff > 0:
					if testName in ['edvt']:
						mesTest = testName
					try:	
						if tree.xpath('/html/body/div[2]/div[1]/table/tbody/tr[%s]/td[5]/a/i/text()' % (idx+1)):
							failText = tree.xpath('/html/body/div[2]/div[1]/table/tbody/tr[%s]/td[5]/a/i/text()' % (idx+1))
							
							mydriver.quit()
							return failText[0], testName
						else:	# If failure info was not on the history page
							try:
								# Click into fail/invalid link
								mydriver.find_element_by_xpath('/html/body/div[2]/div[1]/table/tbody/tr[%s]/td[1]/a' % (idx+1)).click()
							except:
								f = open('failed.txt', 'wb')
								f.write(mydriver.page_source)
								f.close()
								print 'yup failed: %s \n %s' % (serial, idx)
								tb = sys.exc_info()[2]
								tbinfo = traceback.format_tb(tb)[0]
								# Concatenate information together concerning the error into a message string
								pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
								# Capture the SNs where this function failed
								print "%s \n" % (pymsg)
							
							# Make sure the link to the log has loaded
							logLink = wait.until(EC.element_to_be_clickable((By.XPATH, "/html/body/table/tbody/tr/td[1]/a[2]")))
							# Grab html source code
							html_source = mydriver.page_source
							tree = html.fromstring(html_source)
							listOFails=[]

							## If fail text exists on FAIL Page
							# The html on this page can differ so check for either case
							if tree.xpath('/html/body/ul/li'):
								numOfFails = len(tree.xpath('/html/body/ul/li'))
								failCount=1
								while failCount <= numOfFails:
									failTextShort = tree.xpath('/html/body/ul/li[%s]' % failCount)
									listOFails.append(failTextShort[0].text_content().strip()) 
									failCount+=1
									if (''.join(listOFails) == 'Unknown'):
										logLink.click()
										wait.until(EC.presence_of_element_located((By.XPATH,'/html/body/pre')))
										# Grab html source code
										html_source = mydriver.page_source
										tree = html.fromstring(html_source)
										# Grab the text of the log and convert it to something useful
										logText = tree.xpath('/html/body/pre')
										logText=logText[0].text_content()
										logText = logText 
										
										# If fvt-i or fst-i try to find fail text, if none then make it 'LED Failure'
										if testName in ['fvt-interactive','fst-interactive']:
											if re.search(fvtIerror,logText):
												find = re.search(fvtIerror,logText).group() 
												mydriver.quit()
												return find, testName
											else:
												find = 'LED Failure'
												mydriver.quit()
												return find, testName

										# Check for predefined fail texts
										if re.search(dataReadSearch,logText):
											find = re.search(dataReadSearch,logText)
											find = ''.join(find.groups())
											find = "%s" %find
											mydriver.quit()
											return find, testName
											
										elif re.search(testFailSearch,logText):
											find = re.search(testFailSearch,logText).group() 
											mydriver.quit()
											return find, testName

										elif re.search(pyException,logText):
											find = re.search(pyException,logText).group() 
											find = "'%s" %find
											mydriver.quit()
											return find, testName
											
										elif re.search(cmdFailSearch,logText):
											find = re.search(cmdFailSearch,logText).group() 
											mydriver.quit()
											return find, testName
											
										elif re.search(fatalError,logText):
											find = re.search(fatalError,logText).group() 
											mydriver.quit()
											return find, testName

										elif re.search(devSearch,logText):
											find = re.search(devSearch,logText).group() 
											mydriver.quit()
											return find, testName
											
										elif re.search(testSetError,logText):
											find = re.search(testSetError,logText).group() 
											mydriver.quit()
											return find, testName
											
										else:
											find = 'UNKNOWN: Check logfile manually'
											mydriver.quit()
											return find, testName
												
									else:
										find = ''.join(listOFails)
										mydriver.quit()
										return find, testName

							elif tree.xpath('/html/body/ol'):
								numOfFails = len(tree.xpath('/html/body/ol/li'))
								failCount=1
								while failCount <= numOfFails:
									failTextShort = tree.xpath('/html/body/ol/li[%s]' % failCount)
									listOFails.append(failTextShort[0].text_content()) 
									failCount+=1
									
								find = ''.join(listOFails)
								mydriver.quit()
								return find, testName
								break
							find = 'Non Test Fail'
							testName = 'Non Test Fail'
							mydriver.quit()
							return find, testName
							

					except:
						# # Get the traceback object
						tb = sys.exc_info()[2]
						tbinfo = traceback.format_tb(tb)[0]
						# # Concatenate information together concerning the error into a message string
						pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
						# Capture the SNs where this function failed
						print '%s \n Serial:%s' %(pymsg,serial)
						# # Close the Browser Window
						find = 'Non Test Fail'
						testName = 'Non Test Fail'
						mydriver.quit()	
						return find, testName
						
				else:
					continue

		if len(elementArray) == (idx + 1):
			# NO MATCHING FAILURE WAS FOUND IN PITA...
			find = 'NO MATCHING FAILURE FOUND IN PITA'
			testName = 'NA'
			mydriver.quit()	
			return find, testName

def getFailText2(serial,operator,date,test,flowErr,endTime,s,secondFail,initFail):	
	pitaLoginPayload = {	
			'sn': '%s' %serial,
			'mac': '',
			'dev': ''
			}
	if flowErr:
		debugPrint("FLOW ERRORS FOUND!")
	laterTestFail = 0
	possibleTest = 0
	questionMe = 0
	## Login
	t = s.post(pitaSearchRunUrl, data = pitaLoginPayload)
	
	# ## convert html for lxml
	tree = html.fromstring(t.content)
	# ## Grab element containing list
	unitHistPath = tree.xpath('/html/body/div/table/tr[2]/td[4]/a/@href')[0]
	

	pitaHistUrl = server + unitHistPath
	f = s.get(pitaHistUrl, timeout=60)
	testHTML = BeautifulSoup(f.content)
	tree_hist = html.fromstring(testHTML.prettify())
	
	elementArray = tree_hist.xpath('/html/body/div[2]/div[1]/table/tr')
	
	utcOffset = int(endTime[-3:]) #dstDiff + int(endTime[-3:])
	endTime_datetime = datetime.datetime.strptime(endTime[:-3],"%Y-%m-%d %H:%M:%S")	#DPFV time
	endTime_datetime = endTime_datetime - datetime.timedelta(hours=utcOffset)
	
	for idx, item in enumerate(elementArray):
		
		# Grab the test name and result elements (column 1 & 2)
		
		tname = tree_hist.xpath('/html/body/div[2]/div[1]/table/tr[%s]/td[1]' % (idx+1))
		result = tree_hist.xpath('/html/body/div[2]/div[1]/table/tr[%s]/td[2]' % (idx+1))
		# Store the text of the test name and result elements
		testName = tname[0].text_content().strip()
		testResult = result[0].text_content().strip()
		
		# If we have reached the last FAIL, lets check it out
		if (testResult in ['passed']) and (translateTest(testName,serial) == test):
			questionMe = 1
		if testResult in ['failed','invalid','bug']:
			if (testName in ['edvt','fft']) or (translateTest(testName,serial) == test) or (flowErr) or ((translateTest(testName,serial) == initFail) and secondFail):
				debugPrint("Test fail match! MES: %s | PITA: %s" %(test,testName))
				operator_name = tree_hist.xpath('/html/body/div[2]/div[1]/table/tr[%s]/td[4]' % (idx+1))[0].text.strip()
				debugPrint("Operators! MES: %s | PITA: %s" %(operator,operator_name))
				utcOffset = int(date[-3:]) #dstDiff + int(date[-3:])
				date_datetime = datetime.datetime.strptime(date[:-3],"%Y-%m-%d %H:%M:%S")	#Initial fail time
				date_datetime = date_datetime - datetime.timedelta(hours=utcOffset)
				pitaDate = tree_hist.xpath('/html/body/div[2]/div[1]/table/tr[%s]/td[2]/span' % (idx+1))[0]
				pitaDate = pitaDate.get('title')
				
				testduration = pitaDate.split()[-1].strip("()")
				testduration = testduration.split(":")
				testStartTime = pitaDate.split(' (')[0]
				startTime = datetime.datetime.strptime(testStartTime, "%Y-%m-%d %H:%M:%S")
				
				debugPrint("MES Time: %s | PITA Time: %s" %(str(date_datetime), str(startTime)))
				if (testResult in ['invalid']) or (endTime_datetime > startTime > date_datetime) :
					if date_datetime < startTime < endTime_datetime:
						timeDiff = 1
					else:
						timeDiff = -1
				
				else:
					endTime = startTime + datetime.timedelta(hours=int(testduration[0]),minutes=int(testduration[1]),seconds=int(testduration[2]))
					timeDiff = (date_datetime - endTime).total_seconds() / 60.0
				debugPrint("Heres the timediff: %d" %timeDiff)
				if (endTime_datetime < startTime) and secondFail: # if we get to this, we havent found the failure and have now found the test failed after dgqcFound
					laterTestFail = 1
					
				if (15 > timeDiff > 0) or (laterTestFail):	
					logIssue=''
					if testName in ['edvt','fft']:
						mesTest = testName
					try:	

						if tree_hist.xpath('/html/body/div[2]/div[1]/table/tr[%s]/td[5]/a/i/text()' % (idx+1))[0].strip():
							failText = tree_hist.xpath('/html/body/div[2]/div[1]/table/tr[%s]/td[5]/a/i/text()' % (idx+1))[0].strip()
							return failText, testName
						else:
							unitHistPath = tree_hist.xpath('/html/body/div[2]/div[1]/table/tr[%s]/td[1]/a/@href' % (idx+1))[0].strip()
							pitaFailUrl = server + unitHistPath
							x = s.get(pitaFailUrl, timeout=60)
							testHTML = BeautifulSoup(x.content)
							fail_hist = html.fromstring(testHTML.prettify())
							try:
								logLink = fail_hist.xpath("/html/body/table/tr/td[1]/a[2]/@href")[0].strip()
							except IndexError:
								logIssue = fail_hist.xpath("/html/body/table/tr/td[1]/a[2]/text()")[0].strip()
							listOFails=[]

							## If fail text exists on FAIL Page
							# The html on this page can differ so check for either case
							if fail_hist.xpath('/html/body/ul/li'):
								numOfFails = len(fail_hist.xpath('/html/body/ul/li'))
								failCount=1
								while failCount <= numOfFails:
									failTextShort = fail_hist.xpath('/html/body/ul/li[%s]' % failCount)
									listOFails.append(failTextShort[0].text.strip()) 
									failCount+=1
									with open('ulli_parts.html','ab') as f:
										f.write("%s - %d \n" %(pitaFailUrl,len(failTextShort)))
									if (''.join(listOFails) == 'Unknown'):
										if logIssue:
											return logIssue, testName
										l = s.get(logLink, timeout=60)
										# Grab the text of the log and convert it to something useful
										logText = l.content 
										logText = logText 
										# If fvt-i or fst-i try to find fail text, if none then make it 'LED Failure'
										if testName in ['fvt-interactive','fst-interactive']:
											if re.search(fvtIerror,logText):
												find = re.search(fvtIerror,logText).group()
												return find, testName
											else:
												
												find = 'LED Failure'
												return find, testName

										# Check for predefined fail texts
										if re.search(dataReadSearch,logText):
											find = re.search(dataReadSearch,logText)
											find = ''.join(find.groups())
											find = "%s" %find
											return find, testName
											
										elif re.search(testFailSearch,logText):
											find = re.search(testFailSearch,logText).group() 
											return find, testName

										elif re.search(pyException,logText):
											find = re.search(pyException,logText).group() 
											find = "'%s" %find
											return find, testName
											
										elif re.search(cmdFailSearch,logText):
											find = re.search(cmdFailSearch,logText).group() 
											return find, testName
											
										elif re.search(fatalError,logText):
											find = re.search(fatalError,logText).group() 
											return find, testName

										elif re.search(devSearch,logText):
											find = re.search(devSearch,logText).group() 
											return find, testName
											
										elif re.search(testSetError,logText):
											find = re.search(testSetError,logText).group() 
											return find, testName
										elif re.search(burntemp,logText):
											find = re.search(burntemp,logText).group()
											return find, testName
											
										else:
											find = 'UNKNOWN: Check logfile manually'
											return find, testName
												
									else:
										find = ''.join(listOFails)
										return find, testName

							elif fail_hist.xpath('/html/body/ol'):
								numOfFails = len(fail_hist.xpath('/html/body/ol/li'))
								failCount=1
								while failCount <= numOfFails:
									failTextShort = fail_hist.xpath('/html/body/ol/li[%s]/text()' % failCount)
									
									failTextShort = ' '.join(failTextShort)
									failTextShort = ' '.join(failTextShort.split())
									listOFails.append(failTextShort.strip()) 
									failCount+=1
									
								find = ''.join(listOFails)
								return find, testName
								break
							find = 'Non Test Fail'
							testName = 'Non Test Fail'
							return find, testName
					except:
						# # Get the traceback object
						tb = sys.exc_info()[2]
						tbinfo = traceback.format_tb(tb)[0]
						# # Concatenate information together concerning the error into a message string
						pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
						# Capture the SNs where this function failed
						print '%s \n Serial:%s' %(pymsg,serial)
						# # Close the Browser Window
						find = 'Unable to get Failure Text'
						return find, testName
						
				else:
					if len(elementArray) == (idx + 1):
						# NO MATCHING FAILURE WAS FOUND IN PITA...
						find = 'NO MATCHING FAILURE FOUND IN PITA'
						if questionMe:
							testName = '1110095'
						else:
							testName = 'NA'
						return find, testName
					continue
		if len(elementArray) == (idx + 1):
			# NO MATCHING FAILURE WAS FOUND IN PITA...
			find = 'NO MATCHING FAILURE FOUND IN PITA'
			if questionMe:
				testName = '1110095'
			else:
				testName = 'NA'
			return find, testName

def getFailText3(serial,operator,date,test,flowErr,endTime,s,secondFail,initFail):
	raise(requests.exceptions.ConnectionError)

def getWeekNumber(date):
	utcOffset = dstDiff + int(date[-3:])
	date_datetime = datetime.datetime.strptime(date[:-3],"%Y-%m-%d %H:%M:%S")	#Initial fail time
	date_datetime = date_datetime - datetime.timedelta(hours=utcOffset)
	if len(str(date_datetime.date().isocalendar()[1])) == 1:
		weekNum_str = "0" + str(date_datetime.date().isocalendar()[1])
	else:
		weekNum_str = str(date_datetime.date().isocalendar()[1])
	weekNum = str(date_datetime.year) + "_W" + weekNum_str
	return weekNum

def getQuarter(dpfvTime):
	from_zone = tz.gettz('UTC')
	to_zone = tz.tzlocal()
	utcOffset = int(dpfvTime[-3:]) #dstDiff + int(endTime[-3:])
	dpfvTime = datetime.datetime.strptime(dpfvTime[:-3],"%Y-%m-%d %H:%M:%S")
	dpfvTime = dpfvTime - datetime.timedelta(hours=utcOffset)
	# Tell the datetime object that it's in UTC time zone since 
	dpfvTime = dpfvTime.replace(tzinfo=from_zone)
	# Convert time zone
	dpfvTime = dpfvTime.astimezone(to_zone)
	quarter = str(dpfvTime.year) + "_Q" + str(quarters_dict[calendar.month_name[dpfvTime.month]])
	return quarter

def getMonth(dpfvTime):
	from_zone = tz.gettz('UTC')
	to_zone = tz.tzlocal()
	utcOffset = int(dpfvTime[-3:])
	dpfvTime = datetime.datetime.strptime(dpfvTime[:-3],"%Y-%m-%d %H:%M:%S")
	dpfvTime = dpfvTime - datetime.timedelta(hours=utcOffset)
	# Tell the datetime object that it's in UTC time zone since 
	# datetime objects are 'naive' by default
	dpfvTime = dpfvTime.replace(tzinfo=from_zone)
	# Convert time zone
	dpfvTime = dpfvTime.astimezone(to_zone)
	if dpfvTime.month < 10:
		month_str = "0" + str(dpfvTime.month)
	month = str(dpfvTime.year)+ "_M" + month_str
	return month
def lastCheck(serial,failKey,dpfvKey,dpfvFound_prev,dpfvHits):
	
	if len(dbs.query(Unit).filter(Unit.serial == serial, Unit.failKey == failKey).all()) > 0:
		existingUnit = dbs.query(Unit).filter(Unit.serial == serial, Unit.failKey == failKey).first()
		
			
		if int(existingUnit.dpfvKey) < int(dpfvKey):
			return 0
		elif int(existingUnit.dpfvKey) > int(dpfvKey):
			dblock.acquire()
			dbs.query(Unit).filter_by(id=existingUnit.id).delete()
			dbs.commit()
			dblock.release()
	
		
	if len(dpfvHits) > 1:		
		print "DPFVHITS: %s" %dpfvHits 
	for dpfv in dpfvHits:
		if (str(dpfvKey) not in str(dpfv)) and (len(dbs.query(Unit).filter(Unit.serial == serial, Unit.dpfvKey == dpfv).all()) > 0):
			existingUnit = dbs.query(Unit).filter(Unit.serial == serial, Unit.dpfvKey == dpfv).first()
			dblock.acquire()
			dbs.query(Unit).filter_by(id=existingUnit.id).delete()
			dbs.commit()
			dblock.release()
			
	return 1
			
def checkMESHist(toDoList,serial,json_data,employee,s):
	sem.acquire()
	for item in toDoList:
		csvCheck = 0
		initfailKey		= ''
		initfailTime	= ''
		failedTest 		= ''
		failTime		= ''
		failKey			= ''
		pitaTestName 	= ''
		fix				= ''
		user			= employee
		dpfvKey 		= item[0]	
		dpfvDate 		= item[1]
		partNumber		= item[2]
		bgarKey 		= ''
		bgarTime 		= ''
		dgqcKey			= ''
		dgqcTime		= ''
		passKey			= ''
		passTime		= ''
		postFailMesTest	= ''
		postFailPitaTest= ''
		postFailText	= ''
		currentRunningTest = ''
		resultType		= 'DBUG'
		comment			= ''
		deviations		= ''
		postFailOperator = ''
		edvtTest		= ''
		invalid_flag = 0
		flowErr = 0

		try:
			if len(dbs.query(Unit).filter(Unit.serial == serial, Unit.dpfvKey == dpfvKey).all()) > 0:
				unitExists = 1
				initUnitExists = 1
				existingUnit = dbs.query(Unit).filter(Unit.serial == serial, Unit.dpfvKey == dpfvKey).first()
			else:
				unitExists = 0
				initUnitExists = 0
		except sqlalchemy.exc.OperationalError as errc:
			print ("Sqlite3 error:",errc)
			print "\n Adding to list"
			errlist = [item]
			connectErrorList.append([errlist,serial,json_data,employee,s])	
			sem.release()
			return
		repairedDefKeys=[]
		dpfvFound = 0
		dpfvFound_prev = 0
		dgqcFound = 0
		possInvalid = 0
		failText = "TBC'd"
		
		testMode = 0		#0- Not in test, 1- undergoind previous test, 2- undergoing failed test
		histLength = len(json_data['data']['activity'])
		dpfvHits = []
		

		
		for idx,activity in enumerate(json_data['data']['activity']):
			
			# Get info for Last Test Run and clear variables
			if (dpfvFound == 0) and ((activity['process_name'] in list_of_tests) or (possInvalid and ((activity['action_description']=='PASS to') and (activity['data2'] in list_of_tests)))):									# Collect repair keys until DPFV and failed test are found
				prevFailedTest	= failedTest
				prevInitfailTime= initfailTime
				prevInitfailKey	= initfailKey
				
				# Catch Invalid
				if (activity['action_description'] == 'PASS to') and (activity['data2'] in testOrderdict):
					invalid_flag	= 1
					failedTest 		= activity['data2']
					failOperator	= activity['operator']
					initfailTime 		= activity['ts']
					initfailKey			= activity['activity_key']
					edvtTest		= activity['process_name']
					csvCheck = 0
				
				else:
					csvCheck = 1
					
					failedTest 		= activity['process_name']
					failOperator	= activity['operator']
					initfailTime 		= activity['ts']
					initfailKey			= activity['activity_key']
					data2 		= activity['data2']
					actDesc = activity['action_description']
				
				repairedDefKeys	= []	# Reset defects for this failure
				dpfvHits = []
				dpfvKey 		= item[0]	
				dpfvDate 		= item[1]
				user 			= employee
				bgarKey 		= ''
				bgarTime 		= ''
				resultType		= 'DBUG'
				comment 		= ''
				dpfvFound_prev 	= 0
				flowErr = 0
				possInvalid = 0
				if not initUnitExists:
					unitExists = 0
			
			if (dpfvFound == 0) and ((activity['data1']=='DGQC Debug QC') and (activity['action_description']=='PASS to')):
				possInvalid = 1
			
			if (dpfvFound == 0) and (activity['action_description'] in ['Flow error']):
				flowErr = 1
				comment = comment + "FlowError found\n"
			# Grab any 'Repair Defect' activity KEYS after the failed Test
			if failedTest and (activity['action_description'] == 'Repair Defect'):									# Collect repair keys for the current failed test
				repairedDefKeys.append(activity['activity_key'])
				
			# Locate the DPFV activity and get the failure text
			if (dpfvFound == 0) and activity['activity_key'] == int(item[0]):															# Find the "PASS TO DPFV" activity
				dpfvFound = 1
				if csvCheck:
					csvSem.acquire()
					with open('test.csv','ab') as f:
						clusterwriter = csv.writer(f)
						clusterwriter.writerow([serial,initfailKey,failedTest,actDesc,data2,possInvalid])
					csvSem.release()
				dpfvTime = activity['ts']
				debugPrint("Found DPFV")
				debugPrint("Exist: %d" %unitExists)
				if unitExists:
					debugPrint("Existing Result: %s" %existingUnit.resultType)
				if unitExists and (existingUnit.resultType in ['PASS','FAIL']) and (existingUnit.failText not in ['NO MATCHING FAILURE FOUND IN PITA']):
					debugPrint("Type: %s" %existingUnit.resultType)
					debugPrint("This failure is already marked as %s, stopping..." %existingUnit.resultType)
					sem.release()
					break
				try:
					if not unitExists or (unitExists and ((not existingUnit.failText) or (existingUnit.failText in ['NO MATCHING FAILURE FOUND IN PITA']))):
						debugPrint("Getting Failure Text")
						secondFail=0
						if useReq:
							failText, pitaTestName = getFailText2(serial,failOperator,initfailTime,failedTest,flowErr,dpfvTime,s,secondFail,failedTest)					# Get Fail Text
						else:
							failText, pitaTestName = getFailText(serial,failOperator,initfailTime,failedTest,flowErr,dpfvTime)
						if pitaTestName in ['edvt']:
							failedTest = pitaTestName
						if failText in ['NO MATCHING FAILURE FOUND IN PITA']:
							failText, pitaTestName = getFailText2(serial,failOperator,prevInitfailTime,prevFailedTest,flowErr,dpfvTime,s,secondFail,failedTest)
							if failText in ['NO MATCHING FAILURE FOUND IN PITA']:
								pass
								
							else:
								failedTest 		= prevFailedTest
								initfailTime 	= prevInitfailTime
								initfailKey		= prevInitfailKey
				except TypeError:
					# # Get the traceback object
					tb = sys.exc_info()[2]
					tbinfo = traceback.format_tb(tb)[0]
					# # Concatenate information together concerning the error into a message string
					pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
					# Capture the SNs where this function failed
					print '%s \n Serial:%s' %(pymsg,serial)
					print serial
					print failOperator
					print initfailTime
					print failedTest
					print failText
					print pitaTestName
					print "#######################"
					sem.release()
					sys.exit(1)
				except (requests.exceptions.ConnectionError, requests.exceptions.RequestException) as errc:
					print ("Error Connecting:",errc)
					print "\n Adding to list"
					errlist = [item]
					connectErrorList.append([errlist,serial,json_data,employee,s,secondFail])
					sem.release()
					return
				
			# Watch for any DPFV activities prior to our initial DPFV activity and after the Last Failed Test
			elif (dpfvFound == 0) and (activity['data1']=='DPFV Depop FVT') and ((activity['action_description'] == 'MOVE to') or (activity['action_description'] == 'PASS to')):
				if activity['operator'] in empList:
					user			= activity['operator']
				dpfvKey 		= activity['activity_key']
				dpfvDate		= activity['ts']
				dpfvHits.append(dpfvKey)
				dpfvFound_prev = 1

				debugPrint("found a previous DPFV: %s" %dpfvKey)
				if (not unitExists) and (len(dbs.query(Unit).filter(Unit.serial == serial, Unit.dpfvKey == dpfvKey).all()) > 0):
					
					existingUnit = dbs.query(Unit).filter(Unit.serial == serial, Unit.dpfvKey == dpfvKey).first()
					debugPrint("Exist type: %s" %existingUnit.resultType)
					debugPrint("Changing unitExists to 1")
					unitExists = 1

				comment = comment + 'Found Prior DPFV for this Failure.  Old Oper: %s\n' %employee
				
			# Locate the DGQC activity after our initial DPFV activity
			if dpfvFound and (activity['data1']=='DGQC Debug QC') and (activity['action_description']=='PASS to'):	# Find the "PASS TO DGQC" activity
				
				
				dgqcFound = 1
				dgqcTime = activity['ts']
				dgqcKey = activity['activity_key']
				# Check if this DGQCKey already exists for this serial, if so check if the DPFVKey is greater than or less than the existing
				if (len(dbs.query(Unit).filter(Unit.serial == serial, Unit.dgqcKey == dgqcKey).all()) > 0):
					existingUnit = dbs.query(Unit).filter(Unit.serial == serial, Unit.dgqcKey == dgqcKey).first()
					if (existingUnit.resultType in ['PASS','FAIL']) and (existingUnit.failText not in ['NO MATCHING FAILURE FOUND IN PITA']):		# Because if its 'TEST' or 'DEBUG' we want to keep searching
						if int(existingUnit.dpfvKey) < int(dpfvKey):
							sem.release()
							break
							pass
						else:
							dblock.acquire()
							dbs.query(Unit).filter_by(id=existingUnit.id).delete()
							
							dbs.commit()
							dblock.release()
							# Don't REMOVE dbs_local, we still need it! 
						
				debugPrint("Found DGQC")
				debugPrint("DGQCkey: %s" %dgqcKey)
				debugPrint("DefKeys: %s" %repairedDefKeys)
				if (not unitExists) or (unitExists and not existingUnit.fix):
					debugPrint("Getting FIX")
					fix = getFix(json_data,repairedDefKeys)
				else:
					debugPrint("Using Existing Fix")
					fix = existingUnit.fix
				resultType = 'TEST'
				testMode = 0
				
			# Locate the BGAR activity after our initial DPFV activity
			if dpfvFound and (activity['data1']=='BGAR  BGA RWK') and ((activity['action_description'] == 'MOVE to') or (activity['action_description'] == 'PASS to')):	# Find the "PASS TO DGQC" activity
				bgarKey = activity['activity_key']
				bgarTime = activity['ts']
				resultType = 'BGAR'
				
			# Locate the BGAR activity before our initial DPFV activity and AFTER any previous DPFV Activity
			elif (dpfvFound == 0) and dpfvFound_prev and (activity['data1']=='BGAR  BGA RWK') and ((activity['action_description'] == 'MOVE to') or (activity['action_description'] == 'PASS to')):
				# Need to check if this is a debug employee, assuming it is for now
				bgarKey = activity['activity_key']
				bgarTime = activity['ts']
				resultType = 'BGAR'
			
			# Check if ANY test are run AFTER our initial DPFV
			if dpfvFound and (activity['action_description'] == 'PASS to') and (activity['data2'] in testOrderdict) and (activity['data1'] not in ['FAQC Final QC Ar']):		# If unit is being passed to a test
				try:
					currentRunningTest = activity['data2']
					
					# Check if the unit passed its FAILED test
					if failedTest == 'edvt':
						failedTestValue = testOrderdict[edvtTest]
					else:
						try:	# check if the test is part of the dict, if not try to convert the pita test to mes test
							failedTestValue = testOrderdict[failedTest]
						except KeyError:
							failedTestValue = testOrderdict[translateTest(pitaTestName,serial)]
					if testOrderdict[activity['data2']] > failedTestValue: #testOrderdict[failedTest]:								# If unit is going to later test, then it passed
						if (not unitExists) or (unitExists and not existingUnit.fix):
							debugPrint("Getting FIX")
							fix = getFix(json_data,repairedDefKeys)
						else:
							debugPrint("Using Existing Fix")
							fix = existingUnit.fix
						currentRunningTest = ''
						#IT PASSED!!!!

						passTime = activity['ts']
						passKey = activity['activity_key']
						if failText in ['NO MATCHING FAILURE FOUND IN PITA']:
							resultType = 'PASSq'
						else:
							resultType = 'PASS'
						screenlock.acquire()
						print "##### UNIT PASS: %s #####	%s" %(serial,resultType)
						screenlock.release()
					
						checkMe = lastCheck(serial,initfailKey,dpfvKey,dpfvFound_prev,dpfvHits)
						if checkMe:
							if unitExists:
								if (not existingUnit.failText) or (existingUnit.failText in ['NO MATCHING FAILURE FOUND IN PITA']):
									existingUnit.failText 		= failText
									orgFailNoNum 				= (failText.translate(None, digits))
									existingUnit.orgFailNoNum 	= orgFailNoNum
									existingUnit.mesTest		= failedTest
									existingUnit.failTime		= initfailTime
									existingUnit.failKey		= initfailKey
								existingUnit.fix 			= fix
								existingUnit.bgarKey 		= bgarKey
								existingUnit.bgarTime 		= bgarTime
								existingUnit.dgqcKey 		= dgqcKey
								existingUnit.dgqcTime 		= dgqcTime
								existingUnit.postFailMesTest	= currentRunningTest
								existingUnit.passFailKey 	= passKey
								existingUnit.passFailTime 	= passTime
								existingUnit.postFailPitaTest	= postFailPitaTest
								existingUnit.postFailText	= postFailText
								existingUnit.resultType 	= resultType
									
								existingUnit.comment 		= comment
								dblock.acquire()
								dbs.commit()
								dblock.release()
								
							else:
								weekNum = getWeekNumber(dpfvTime)
								quarter = getQuarter(dpfvTime)
								month	= getMonth(dpfvTime)
								orgFailNoNum = (failText.encode('utf-8').translate(None, digits)).decode('utf-8')
								unit = Unit(serial,failedTest,pitaTestName,initfailKey,
											initfailTime, failText,user,dpfvKey,dpfvTime,
											partNumber,currentRunningTest,weekNum,fix,bgarKey,bgarTime,dgqcKey,
											dgqcTime,passKey,passTime,postFailPitaTest,
											postFailText,resultType,comment,deviations,quarter,orgFailNoNum,month)
								dblock.acquire()
								dbs.add(unit)
								
								dbs.commit()
								dblock.release()
						sem.release()
						break
					
					# Check if the unit passed is testing previous tests
					elif testOrderdict[activity['data2']] < failedTestValue: # Unit is performing a previous test
						testMode = 1
						currentRunningTest = activity['data2']
						postFailTime 		= activity['ts']
					# else it is running the failed test again
					else:
						testMode = 2													# Unit is performing the previously failed test
						currentRunningTest = activity['data2']
						postFailTime 		= activity['ts']

				except KeyError:
					print "## ERROR ##"
					tb = sys.exc_info()[2]
					tbinfo = traceback.format_tb(tb)[0]
					# # Concatenate information together concerning the error into a message string
					pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
					# Capture the SNs where this function failed
					print '%s \n Serial:%s' %(pymsg,serial)
					print activity['data2']
					print failedTest
					print activity['activity_key']
					print "###########"
			
			# If the unit is currently passed DGQC and being tested
			if (testMode > 0):
				if 'TEST SERVER:' in (activity['data']):
					postFailOperator = activity['operator']
			
				# Check if the unit was moved back to DEBUG, if so it failed
				if ((activity['action_description'] == 'MOVE to') or (activity['action_description'] == 'PASS to')) and (activity['data1'] in ['DFVT Debug FVT','DPFV Depop FVT']) and (activity['data1'] != 'DGQC Debug QC'):	#,'DPFV Depop FVT' #('Debug' in activity['data1'])
					if (not unitExists) or (unitExists and not existingUnit.fix):
						debugPrint("Getting FIX")
						fix = getFix(json_data,repairedDefKeys)
					else:
						debugPrint("Using Existing Fix")
						fix = existingUnit.fix
					
					if activity['data1'] in ['DPFV Depop FVT']:
						comment = comment + "DPFV_fail\n"
					else:
						comment = comment + "DFVT_fail\n"
					# IT FAILED!!!!
							
					failTime = activity['ts']
					failKey = activity['activity_key']
					if failText in ['NO MATCHING FAILURE FOUND IN PITA']:
						resultType = 'FAILq'
					else:
						resultType = 'FAIL'
					
					secondFail = 1
					try:
						if useReq:
							postFailText, postFailPitaTest = getFailText2(serial,postFailOperator,postFailTime,currentRunningTest,0,failTime,s,secondFail,failedTest)
						else:
							postFailText, postFailPitaTest = getFailText(serial,postFailOperator,postFailTime,currentRunningTest,0,failTime)
					
					except (requests.exceptions.ConnectionError,requests.exceptions.RequestException) as errc:
						print ("Error Connecting:",errc)
						print "\n Adding to list"
						errlist = [item]
						connectErrorList.append([errlist,serial,json_data,employee,s,secondFail])
						sem.release()
						return
					except:
						# # Get the traceback object
						tb = sys.exc_info()[2]
						tbinfo = traceback.format_tb(tb)[0]
						# # Concatenate information together concerning the error into a message string
						pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
						# Capture the SNs where this function failed
						print '%s \n Serial:%s' %(pymsg,serial)
						print serial
						print "postfailoper: %s" %postFailOperator
						print "postfailtime: %s" %postFailTime
						print "postFailTest: %s" %currentRunningTest
						print "post Fail Key: %s" %failKey
						print "#######################"
						sem.release()
						sys.exit(1)
						
						
					if postFailPitaTest in ['1110095']:
						# print "Second Failure %s for %s, NOT found in PITA, continuing" %(failKey,serial)
						failTime = ''
						failKey = ''
						resultType = ''
						postFailText = ''
						postFailPitaTest = ''
						# Check if we have come to the end of the history
						if dpfvFound and ((idx + 1) >= histLength):
							currentRunningTest = ''
							if (not unitExists) or (unitExists and not existingUnit.fix):
								debugPrint("Getting FIX")
								fix = getFix(json_data,repairedDefKeys)
							else:
								debugPrint("Using Existing Fix")
								fix = existingUnit.fix
							
							screenlock.acquire()
							print "### UNIT IN DEBUG: %s ###" %serial
							screenlock.release()
							try:
								checkMe = lastCheck(serial,initfailKey,dpfvKey,dpfvFound_prev,dpfvHits)
							except sqlalchemy.exc.OperationalError as errc:
								print ("Sqlite3 error:",errc)
								print "\n Adding to list"
								errlist = [item]
								connectErrorList.append([errlist,serial,json_data,employee,s])	
								sem.release()
								return
							if checkMe:
								if unitExists:
									existingUnit.fix 			= fix
									existingUnit.bgarKey 		= bgarKey
									existingUnit.bgarTime 		= bgarTime
									existingUnit.dgqcKey 		= dgqcKey
									existingUnit.dgqcTime 		= dgqcTime
									existingUnit.resultType 	= resultType
									existingUnit.comment 		= comment
									dblock.acquire()
									dbs.commit()
									dblock.release()
								else:
									weekNum = getWeekNumber(dpfvTime)
									quarter = getQuarter(dpfvTime)
									month	= getMonth(dpfvTime)
									orgFailNoNum = (failText.translate(None, digits))
									unit 	= Unit(serial,failedTest,pitaTestName,initfailKey,
												initfailTime, failText,user,dpfvKey,dpfvTime,
												partNumber,currentRunningTest,weekNum,fix,bgarKey,bgarTime,dgqcKey,
												dgqcTime,failKey,failTime,postFailPitaTest,
												postFailText,resultType,comment,deviations,quarter,orgFailNoNum,month)
									
									dblock.acquire()		# Lock DB for Writing
									dbs.add(unit)		# Stage info
									dbs.commit()		# Commit to DB
									dblock.release()		# Unlock DB
							sem.release()				# Give back the sem
							break
							
						continue
					screenlock.acquire()
					print "### UNIT FAIL: %s ###	%s" %(serial,resultType)
					screenlock.release()
					try:
						checkMe = lastCheck(serial,initfailKey,dpfvKey,dpfvFound_prev,dpfvHits)
					except sqlalchemy.exc.OperationalError as errc:
						print ("Sqlite3 error:",errc)
						print "\n Adding to list"
						errlist = [item]
						connectErrorList.append([errlist,serial,json_data,employee,s])	
						sem.release()
						return
					if checkMe:
						if unitExists:
							if (not existingUnit.failText) or (existingUnit.failText in ['NO MATCHING FAILURE FOUND IN PITA']):
								existingUnit.failText 		= failText
								orgFailNoNum 				= (failText.translate(None, digits))
								existingUnit.orgFailNoNum 	= orgFailNoNum
								existingUnit.mesTest		= failedTest
								existingUnit.failTime		= initfailTime
								existingUnit.failKey		= initfailKey
							existingUnit.fix 			= fix
							existingUnit.bgarKey 		= bgarKey
							existingUnit.bgarTime 		= bgarTime
							existingUnit.dgqcKey 		= dgqcKey
							existingUnit.dgqcTime 		= dgqcTime
							existingUnit.postFailMesTest	= currentRunningTest
							existingUnit.passFailKey 	= failKey
							existingUnit.passFailTime 	= failTime
							existingUnit.postFailPitaTest	= postFailPitaTest
							existingUnit.postFailText	= postFailText
							existingUnit.resultType 	= resultType
							existingUnit.comment 		= comment
							dblock.acquire()
							dbs.commit()
							dblock.release()
						else:
							weekNum = getWeekNumber(dpfvTime)
							quarter = getQuarter(dpfvTime)
							month	= getMonth(dpfvTime)
							orgFailNoNum = (failText.encode('utf-8').translate(None, digits)).decode('utf-8')
							unit = Unit(serial,failedTest,pitaTestName,initfailKey,
											initfailTime, failText,user,dpfvKey,dpfvTime,
											partNumber,currentRunningTest,weekNum,fix,bgarKey,bgarTime,dgqcKey,
											dgqcTime,failKey,failTime,postFailPitaTest,
											postFailText,resultType,comment,deviations,quarter,orgFailNoNum,month)
							dblock.acquire()
							dbs.add(unit)
							
							dbs.commit()
							dblock.release()
					sem.release()	
					break
				if activity['action_description'] == 'Defect seq':
					postFailOperator	= activity['operator']
					postFailTime 		= activity['ts']
					debugPrint("Failed Test")
					
			# Check if we have come to the end of the history
			if dpfvFound and ((idx + 1) >= histLength):
				currentRunningTest = ''
				if (not unitExists) or (unitExists and not existingUnit.fix):
					debugPrint("Getting FIX")
					fix = getFix(json_data,repairedDefKeys)
				else:
					debugPrint("Using Existing Fix")
					fix = existingUnit.fix
				
				screenlock.acquire()
				print "### UNIT IN DEBUG: %s ###	%s" %(serial,resultType)
				screenlock.release()
				try:
					checkMe = lastCheck(serial,initfailKey,dpfvKey,dpfvFound_prev,dpfvHits)
				except sqlalchemy.exc.OperationalError as errc:
					print ("Sqlite3 error:",errc)
					print "\n Adding to list"
					errlist = [item]
					connectErrorList.append([errlist,serial,json_data,employee,s])	
					sem.release()
					return
				if checkMe:
					if unitExists:
						existingUnit.fix 			= fix
						existingUnit.bgarKey 		= bgarKey
						existingUnit.bgarTime 		= bgarTime
						existingUnit.dgqcKey 		= dgqcKey
						existingUnit.dgqcTime 		= dgqcTime
						existingUnit.resultType 	= resultType
						existingUnit.comment 		= comment
						dblock.acquire()
						try:
							dbs.commit()
						except:
							tb = sys.exc_info()[2]
							tbinfo = traceback.format_tb(tb)[0]
							# # Concatenate information together concerning the error into a message string
							pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
							# Capture the SNs where this function failed
							screenlock.acquire()
							print "#############################=ERROR=#####################################"
							print '%s \n Serial:%s\nInitFailKey:%s' %(pymsg,serial,initfailKey)
							print "#########################################################################"
							screenlock.release()
						dblock.release()
					else:
						weekNum = getWeekNumber(dpfvTime)
						quarter = getQuarter(dpfvTime)
						month	= getMonth(dpfvTime)
						orgFailNoNum = (failText.translate(None, digits))
						unit 	= Unit(serial,failedTest,pitaTestName,initfailKey,
									initfailTime, failText,user,dpfvKey,dpfvTime,
									partNumber,currentRunningTest,weekNum,fix,bgarKey,bgarTime,dgqcKey,
									dgqcTime,failKey,failTime,postFailPitaTest,
									postFailText,resultType,comment,deviations,quarter,orgFailNoNum,month)
						
						dblock.acquire()		# Lock DB for Writing
						dbs.add(unit)		# Stage info
						dbs.commit()		# Commit to DB
						dblock.release()		# Unlock DB
				sem.release()					# Give back the sem
				break

	sem.release()


def updateExisting(s):
	
	units = dbs.query(Unit).filter(~Unit.resultType.in_(['PASS','FAIL','PASSq','FAILq'])).all()
	print "Checking %d units" %len(units)
	for entry in units:
		json_data = ''
		print "Updating serial: %s" %entry.serial
		jsonInfo = requests.get(mesHistoryURL %entry.serial, timeout=30)
		json_data = jsonInfo.json()
		item = [[entry.dpfvKey,entry.dpfvTime,entry.partNumber]]
		
		t = Thread(target=checkMESHist, args=(item,entry.serial,json_data,entry.user,s, ))
		t.daemon= True
		updateExistingThreadList.append(t)
	for t in updateExistingThreadList:
		t.start()
	for t in updateExistingThreadList:
		t.join()

def copytree(src, dst, symlinks=False, ignore=None):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)	
	
print "Getting Employees"
empNumbers = []
empList		= []
empNumbers, empList = getDebugEmployees()
if not empList:
	for value in dbs.query(Tech.empID).distinct():
		empList.append(value.empID)
		empNumbers.append(value.empID + '\n')

print "Done\nGetting Touches"
getTechHist(empNumbers)	
print "Done"
with open('test.csv','wb') as f:
	clusterwriter = csv.writer(f)
	clusterwriter.writerow(['serial','initfailKey',"activity['process_name']","activity['action_description']","activity['data2']","possInvalid"])
		
start = datetime.datetime.now()

with requests.Session() as s:
	s.cookies.clear()
	s.headers.update({
		'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
		'Accept-Language': 'en-US,en;q=0.9',
		'Cache-Control': 'max-age=0',
		})
	print "Checking old Entries"
	updateExisting(s)
	print "DONE!"

	if os.path.exists(dlFolder):
		print "File Exists"
	else:
		print "File Does Not Exist"
		os.mkdir(dlFolder)
	parsetechHist(dpfvHistory)

	currentThreadList = []
	for employee in touchDict:
		print employee
		currentThreadList = []
		print "Connection Errors So Far: %d" %len(connectErrorList)
		print "Checking Employee: %s" %employee
		for serial in touchDict[employee]:
			json_data = ''
			serial_stripped = serial.strip()
			jsonInfo = requests.get(mesHistoryURL %serial_stripped, timeout=30)
			try:
				json_data = jsonInfo.json()
			except:
				tb = sys.exc_info()[2]
				tbinfo = traceback.format_tb(tb)[0]
				# Concatenate information together concerning the error into a message string
				pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
				print pymsg
				print "Serial: %s" %serial_stripped
				print "Link: %s" %(mesHistoryURL %serial_stripped)
				print "Trying again"
				try:
					json_data = jsonInfo.json()
				except:
					tb = sys.exc_info()[2]
					tbinfo = traceback.format_tb(tb)[0]
					# Concatenate information together concerning the error into a message string
					pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
					print pymsg
					continue
			t = Thread(target=checkMESHist, args=(touchDict[employee][serial],serial_stripped,json_data,employee,s, ))
			t.daemon= True
			currentThreadList.append(t)
		for t in currentThreadList:
			t.start()
		for t in currentThreadList:
			t.join()

print " Total Connection Errors: %d" %len(connectErrorList)
while len(connectErrorList) > 0:
	currentThreadList = []
	for item in connectErrorList:
		t = Thread(target=checkMESHist, args=(item[0],item[1],item[2],item[3],item[4], ))
		t.daemon= True
		currentThreadList.append(t)
	connectErrorList = []
	for t in currentThreadList:
		t.start()
	for t in currentThreadList:
		t.join()
	print " Total Connection Errors: %d" %len(connectErrorList)
dbs.remove()		
print "Copying Main DB to Cloud"
shutil.copy2(dataBasePath_local,dataBasePath_cloud)
		
		
print 'Updating Totals for Site'
os.system(updateScript)

print "Copying Totals DB to Cloud"
shutil.copy2(dataBaseTotalsPath_local,dataBasePath_cloud)

print "Copying Json data Folder to Cloud"
copytree(dataBaseJsonPath_local,dataBaseJsonPath_cloud)

print "Letting files upload to cloud before triggering API..."
time.sleep(180)


try:
	print "Triggering Not Doug to copy the new files"
	results = requests.get(GETDBURL, timeout=60)
	print "Result: %s" %results
except:
	print "Not doug is not responding...."

end = datetime.datetime.now()
print "Seconds to complete: %d" %(end-start).seconds	
		