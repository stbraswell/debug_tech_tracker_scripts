import flask
from flask import request, jsonify
from flask_cors import CORS, cross_origin
import sqlite3
import pytz
import datetime
import time
import calendar
import collections
import re
import shutil
import os
import json
from waitress import serve
import logging
import sys
import traceback
from paste.translogger import TransLogger 

logger = logging.getLogger('waitress')
logger.setLevel(logging.DEBUG)
####################################################
##						VARS
####################################################
app = flask.Flask(__name__)
app.config["DEBUG"] = True
format = '%(status)s %(REQUEST_METHOD)s [%(time)s] -%(REMOTE_ADDR)s- %(REQUEST_URI)s'
tz = pytz.timezone("America/Los_Angeles")
dataBase_techTracker = 'techTracker.db'
dataBase_SGD_Debub	= 'C:/Users/doug_debug/Desktop/Arista_scripts/SGD/Vulcan_Reporting/debugTracker.db'
dataBase_totals			=	'techTracker_totals.db'
apiDoc = "apiDoc.txt"
quarters_dict = {'January':1,
				'February':1,
				'March':1,
				'April':2,
				'May':2,
				'June':2,
				'July':3,
				'August':3,
				'September':3,
				'October':4,
				'November':4,
				'December':4}			

quarter_num = {1:['01','03'],
				2:['04','06'],
				3:['07','09'],
				4:['10','12']
				}

####################################################
##						FUNCTIONS
####################################################
				
def dict_factory(cursor, row):
	d = {}
	for idx, col in enumerate(cursor.description):
		d[col[0]] = row[idx]
	return d

def midnight():
	the_date = datetime.date.today()  												# Get todays date (Local Time)
	midnight_without_tzinfo = datetime.datetime.combine(the_date, datetime.time())	# Get midnight (Local time)
	midnight_with_tzinfo = tz.localize(midnight_without_tzinfo)						# Add the Local UTC offset to timestamp
	utcTime=midnight_with_tzinfo.astimezone(pytz.utc)								# Calculate the UTC time of local Midnight
	return utcTime

def getQuarterNumbers2(quarter,year):	
	quarterString =year + "_Q" + quarter
	query = 'SELECT * FROM totals WHERE timeframe=?;'
	to_filter = []
	to_filter.append(quarterString)
	conn = sqlite3.connect(dataBase_totals)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	passResults = cur.execute(query, to_filter).fetchall()
	if passResults:
		return passResults[0]['accuracy']
	else:
		return "Not Updated Yet"
	
def getWeekNumbers2(weekNum):
	query = 'SELECT * FROM totals WHERE timeframe=?;'
	to_filter = []
	to_filter.append(weekNum)
	conn = sqlite3.connect(dataBase_totals)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	passResults = cur.execute(query, to_filter).fetchall()
	if passResults:
		return passResults[0]['accuracy']
	else:
		return "Not Updated Yet"
		
def getQuarterNumbers(quarter,year,returnMe):
	months = quarter_num[int(quarter)]
	dstDiff = 8 - time.localtime().tm_isdst
	startDay = datetime.datetime(int(year),int(months[0]),01)
	startDay = startDay + datetime.timedelta(hours=dstDiff)
	week = startDay.isocalendar()[1]
	
	# Get last week number of quarter
	daysInMonth = calendar.monthrange(int(year),int(months[1]))[1]
	lastDay = datetime.datetime(int(year),int(months[1]),daysInMonth)
	lastDay = lastDay + datetime.timedelta(days=1, hours=dstDiff)
	to_filter_pass = []
	to_filter_fail = []
	to_filter_test = []
	to_filter_bgar = []
	to_filter_dbug = []
	query = "SELECT COUNT(serial) FROM trackerData WHERE dpfvTime BETWEEN ? AND ? AND resultType=?" 
	to_filter_pass.extend([str(startDay),str(lastDay),"PASS"])
	to_filter_fail.extend([str(startDay),str(lastDay),"FAIL"])
	if returnMe == 'all':
		to_filter_dbug.extend([str(startDay),str(lastDay),"DBUG"])
		to_filter_test.extend([str(startDay),str(lastDay),"TEST"])
		to_filter_bgar.extend([str(startDay),str(lastDay),"BGAR"])
	conn = sqlite3.connect(dataBase_techTracker)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	passResults = cur.execute(query, to_filter_pass).fetchall()
	failResults = cur.execute(query, to_filter_fail).fetchall()
	if returnMe == 'all':
		dbugResults = cur.execute(query, to_filter_dbug).fetchall()
		testResults = cur.execute(query, to_filter_test).fetchall()
		bgarResults = cur.execute(query, to_filter_bgar).fetchall()
	
	passCount = passResults[0]['COUNT(serial)']
	failCount = failResults[0]['COUNT(serial)']
	if returnMe == 'all':
		dbugCount = dbugResults[0]['COUNT(serial)']
		bgarCount = bgarResults[0]['COUNT(serial)']
		testCount = testResults[0]['COUNT(serial)']
	total = int(passCount) + int(failCount)
	if total > 0:
		percentPass = (int(passCount) / float(total)) * 100
	else: 
		percentPass = 0
	percentPass = round(percentPass,2)
	if returnMe == 'all':
		return passCount,failCount,percentPass,dbugCount,bgarCount,testCount
	elif returnMe == 'passPercent':
		return percentPass
		
def getWeekNumbers(weekNum,returnMe):
	to_filter_pass = []
	to_filter_fail = []
	to_filter_test = []
	to_filter_bgar = []
	to_filter_dbug = []
	query = "SELECT COUNT(serial) FROM trackerData WHERE"
	query += ' week=? AND'
	query += ' resultType=? AND'
	query = query[:-4] + ';'
	to_filter_pass.extend([weekNum,"PASS"])
	to_filter_fail.extend([weekNum,"FAIL"])
	if returnMe == 'all':
		to_filter_dbug.extend([weekNum,"DBUG"])
		to_filter_test.extend([weekNum,"TEST"])
		to_filter_bgar.extend([weekNum,"BGAR"])
	conn = sqlite3.connect(dataBase_techTracker)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	passResults = cur.execute(query, to_filter_pass).fetchall()
	failResults = cur.execute(query, to_filter_fail).fetchall()
	if returnMe == 'all':
		dbugResults = cur.execute(query, to_filter_dbug).fetchall()
		testResults = cur.execute(query, to_filter_test).fetchall()
		bgarResults = cur.execute(query, to_filter_bgar).fetchall()
	
	passCount = passResults[0]['COUNT(serial)']
	failCount = failResults[0]['COUNT(serial)']
	if returnMe == 'all':
		dbugCount = dbugResults[0]['COUNT(serial)']
		bgarCount = bgarResults[0]['COUNT(serial)']
		testCount = testResults[0]['COUNT(serial)']
	total = int(passCount) + int(failCount)
	if total > 0:
		percentPass = (int(passCount) / float(total)) * 100
	else: 
		percentPass = 0
	percentPass = round(percentPass,2)
	if returnMe == 'all':
		return passCount,failCount,percentPass,dbugCount,bgarCount,testCount
	elif returnMe == 'passPercent':
		return percentPass

def getOverallNumbers(returnMe):
	to_filter_pass = []
	to_filter_fail = []
	to_filter_test = []
	to_filter_bgar = []
	to_filter_dbug = []
	query = "SELECT COUNT(serial) FROM trackerData WHERE resultType=? AND fix !='Retest'" 
	to_filter_pass.extend(["PASS"])
	to_filter_fail.extend(["FAIL"])
	if returnMe == 'all':
		to_filter_dbug.extend(["DBUG"])
		to_filter_test.extend(["TEST"])
		to_filter_bgar.extend(["BGAR"])
	conn = sqlite3.connect(dataBase_techTracker)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	passResults = cur.execute(query, to_filter_pass).fetchall()
	failResults = cur.execute(query, to_filter_fail).fetchall()
	if returnMe == 'all':
		dbugResults = cur.execute(query, to_filter_dbug).fetchall()
		testResults = cur.execute(query, to_filter_test).fetchall()
		bgarResults = cur.execute(query, to_filter_bgar).fetchall()

	passCount = passResults[0]['COUNT(serial)']
	failCount = failResults[0]['COUNT(serial)']
	if returnMe == 'all':
		dbugCount = dbugResults[0]['COUNT(serial)']
		bgarCount = bgarResults[0]['COUNT(serial)']
		testCount = testResults[0]['COUNT(serial)']
	total = int(passCount) + int(failCount)
	if total > 0:
		percentPass = (int(passCount) / float(total)) * 100
	else: 
		percentPass = 0
	percentPass = round(percentPass,2)
	if returnMe == 'all':
		return passCount,failCount,percentPass,dbugCount,bgarCount,testCount
	elif returnMe == 'passPercent':
		return percentPass

def getLastQuarter():
	today = datetime.datetime.today().date()
	thisMonth = calendar.month_name[today.month]
	thisQuarter = quarters_dict[thisMonth]
	thisYear = today.year
	lastQuarter = int(thisQuarter) - 1
	if not lastQuarter:
		lastQuarter = 4
		thisYear = thisYear - 1
	year = str(thisYear)
	quarter = str(lastQuarter)
	return quarter,year
	
def getLastWeek():
	today = datetime.datetime.today()
	lastWeek = today - datetime.timedelta(weeks=1)
	if lastWeek.isocalendar()[1] < 10:
		week_str = '0' + str(lastWeek.isocalendar()[1])
	else:
		week_str = str(lastWeek.isocalendar()[1])
	weekNum = str(lastWeek.year) + "_W" + week_str
	return weekNum
	
def getQuarterlyChart(type):
	query = 'SELECT * FROM (SELECT * FROM totals WHERE timeframe LIKE ? ORDER BY timeframe DESC) ORDER BY timeframe ASC ;'
	to_filter = []
	quarterLabels = []
	quarterAccs = []
	quarterTops = collections.OrderedDict()
	quarterTops_list = []
	quarterlyHTML = ''
	numToShow = 4
	
	if type in ['quarter']:
		to_filter.append("20%Q%")
		lyHTML_file="C:/Apache24/htdocs/techTracker/card.html"
		numToShow = 5
	elif type in ['week']:
		to_filter.append("20%W%")
		lyHTML_file="C:/Apache24/htdocs/techTracker/weekCard.html"
		numToShow = 20
	elif type in ['month']:
		to_filter.append("20%M%")
		lyHTML_file="C:/Apache24/htdocs/techTracker/monthCard.html"
		numToShow = 13
	conn = sqlite3.connect(dataBase_totals)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	results = cur.execute(query, to_filter).fetchall()
	with open(lyHTML_file,'rb') as f:
		cardHTML = f.read()
	for idx,item in enumerate(results):
		# Take the data for the last 20 weeks for the Chart
		if type in ['quarter','week']:
			print "This is quarterly Query"
			year = item['timeframe'].split('_')[0]
			quarter_str = item['timeframe'].split('_')[1]
			timeFrame = quarter_str + " " + year
		elif type in ['month']:
			print "This is monthly Query"
			year = item['timeframe'].split('_')[0]
			monthName = calendar.month_name[int(item['timeframe'].split('_')[1][1:])]
			timeFrame= monthName + " " + year
		else:
			timeFrame = item['timeframe']
		print timeFrame
		if idx >= (len(results) - numToShow):
			quarterLabels.append(timeFrame)
			quarterAccs.append(item['accuracy'])
		topTechs = item['topTechs'].strip().split('\n')
		
		quarterlyHTML = cardHTML%(timeFrame,item['accuracy'],item['numPass'],item['numFail'],topTechs[0],topTechs[1],topTechs[2],item['timeframe']) + "\n" + quarterlyHTML
		
	with open('jsTest.txt','wb') as f:
		f.write(quarterlyHTML)
	return jsonify(Labels=quarterLabels,Accs=quarterAccs,html=quarterlyHTML)

def getIndivQuarter(quarter):
	if re.match('[0-9]{4}_Q\d',quarter):
		query 		= 'SELECT * FROM trackerData WHERE quarter=? AND resultType in ("PASS","FAIL") AND NOT fix="Retest";'
		query_all	= 'SELECT * FROM trackerData WHERE quarter=?;'
	elif re.match('[0-9]{4}_W[0-9]{1,2}',quarter):
		query 		= 'SELECT * FROM trackerData WHERE week=? AND resultType in ("PASS","FAIL") AND NOT fix="Retest";'
		query_all	= 'SELECT * FROM trackerData WHERE week=?;'
	elif re.match('[0-9]{4}_M[0-9]{1,2}',quarter):
		query 		= 'SELECT * FROM trackerData WHERE month=? AND resultType in ("PASS","FAIL") AND NOT fix="Retest";'
		query_all	= 'SELECT * FROM trackerData WHERE month=?;'
	to_filter = []
	to_filter.append(quarter)
	conn = sqlite3.connect(dataBase_techTracker)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	results = cur.execute(query, to_filter).fetchall()
	full_user_labels,full_user_productivity = getTechProductivity(query_all, to_filter)
	print "User Labels:"
	full_user_labels.reverse()
	for item in full_user_labels:
		print item
	print "User Productivity:"
	full_user_productivity.reverse()
	for item in full_user_productivity:
		print item
	userdict = collections.defaultdict(list)
	userNames = []
	userPercents = []
	userFailPercents = []
	numPassFail = []
	numberPasses = []
	numberFails = []
	fixList = []
	temp_html = '<br><table style="border: 1px solid black ;border-collapse:collapse;font-size: small;">\n\
						<tr style="background-color: #CFCFA3;">\n\
							<th style="border: 1px solid black ;">Serial Number</th>\n\
							<th style="border: 1px solid black ;">Part Number</th>\n\
							<th style="border: 1px solid black ;">Tech</th>\n\
							<th style="border: 1px solid black ;">Fail Text</th>\n\
							<th style="border: 1px solid black ;">Debug Time</th>\n\
							<th style="border: 1px solid black ;">Failed Test</th>\n\
							<th style="border: 1px solid black ;">Fix</th>\n\
							<th style="border: 1px solid black ;">Pass/Fail</th>\n\
						</tr>'

	color_switch=0
	for item in sorted(results, key=lambda k: k['dpfvTime']):
		if item['user'] in userdict:
			if item['resultType'] in ['PASS']:
				userdict[item['user']][0] += 1
			elif item['resultType'] in ['FAIL']:
				userdict[item['user']][1] += 1
		else:
			if item['resultType'] in ['PASS']:
				userdict[item['user']] = [1,0]
			elif item['resultType'] in ['FAIL']:
				userdict[item['user']] = [0,1]
		if color_switch == 0:
			temp_html = temp_html + '<tr style="background-color: #FFFFDB;"><td style="border: 1px solid black ;">%s</td>\n' % (item['serial'])
			temp_html = temp_html + '<td style="border: 1px solid black ;">%s</td>\n' % (item['partNumber'])
			temp_html = temp_html + '<td style="border: 1px solid black ;">%s</td>\n' % (item['user'])
			temp_html = temp_html + '<td style="border: 1px solid black ;">%s</td>\n' % (item['failText'])
			temp_html = temp_html + '<td style="border: 1px solid black ;">%s</td>\n' % (item['dpfvTime'])
			temp_html = temp_html + '<td style="border: 1px solid black ;">%s</td>\n' % (item['pitaTest'])
			temp_html = temp_html + '<td style="border: 1px solid black ;">%s</td>\n' % (item['fix'])
			temp_html = temp_html + '<td style="border: 1px solid black ;">%s</td></tr>\n' % (item['resultType'])
			color_switch = 1
		else:
			temp_html = temp_html + '<tr style="background-color: #CFCFA3;"><td style="border: 1px solid black ;">%s</td>\n' % (item['serial'])
			temp_html = temp_html + '<td style="border: 1px solid black ;">%s</td>\n' % (item['partNumber'])
			temp_html = temp_html + '<td style="border: 1px solid black ;">%s</td>\n' % (item['user'])
			temp_html = temp_html + '<td style="border: 1px solid black ;">%s</td>\n' % (item['failText'])
			temp_html = temp_html + '<td style="border: 1px solid black ;">%s</td>\n' % (item['dpfvTime'])
			temp_html = temp_html + '<td style="border: 1px solid black ;">%s</td>\n' % (item['pitaTest'])
			temp_html = temp_html + '<td style="border: 1px solid black ;">%s</td>\n' % (item['fix'])
			temp_html = temp_html + '<td style="border: 1px solid black ;">%s</td></tr>\n' % (item['resultType'])
			color_switch = 0
	temp_html = temp_html + "</table>\n" 
	
	for item in userdict:
		passPercent = (userdict[item][0]/float(sum(userdict[item]))) * 100
		passPercent = round(passPercent,2)
		failPercent = round(100 - passPercent,2)
		numPassFail.append([userdict[item][0],userdict[item][1],passPercent,failPercent,item])

	numPassFail.sort(key=lambda x: int(x[0]), reverse=True)
	numberPasses = [x[0] for x in numPassFail]
	numberFails = [x[1] for x in numPassFail]
	userPercents=[x[2] for x in numPassFail]
	userFailPercents=[x[3] for x in numPassFail]
	userNames=[x[4] for x in numPassFail]
	print "Getting top 5"
	fixLabels,fixCounts,xlabels = getTopFive('fix',quarter)
	failLabels,failCounts,xlabels,finalTestDict = getTopFive('orgFailNoNum',quarter)
	
	return jsonify(Quart=quarter,Labels=userNames,Accs=userPercents,Fails=userFailPercents,prod_labels=full_user_labels,prod_nums=full_user_productivity,numPass=numberPasses,numFail=numberFails,table = temp_html,topFixes={"labels":fixLabels,"counts":fixCounts},topFails={"labels":failLabels,"counts":failCounts,"xlabels":xlabels},failsWithTest=finalTestDict) #jsonify(fullResults) #jsonify(table = temp_html)


def getTechProductivity(query,filter):
	conn = sqlite3.connect(dataBase_techTracker)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	results = cur.execute(query, filter).fetchall()
	userdict = collections.defaultdict(list)
	for item in results:
		if item['user'] in userdict:
			userdict[item['user']] += 1
		else:
			userdict[item['user']] = 1
	userNames=[]
	userProductivity = []

	for item in sorted(userdict.items(), key=lambda k: k[1]):
		userNames.append(item[0])
		userProductivity.append(item[1])
	return userNames,userProductivity

def getTopFive(column,timeFrame):
	query = ''
	if (re.match('[0-9]{4}_Q\d',timeFrame)) and (column in ['orgFailNoNum']):
		query = 'SELECT %s,pitaTest,fix FROM trackerData WHERE quarter=? AND resultType in ("PASS") AND NOT fix="Retest";' %column
	elif (re.match('[0-9]{4}_Q\d',timeFrame)):
		query = 'SELECT %s FROM trackerData WHERE quarter=? AND resultType in ("PASS") AND NOT fix="Retest";' %column
	elif (re.match('[0-9]{4}_W[0-9]{1,2}',timeFrame)) and (column in ['orgFailNoNum']):
		query = 'SELECT %s,pitaTest,fix FROM trackerData WHERE week=? AND resultType in ("PASS") AND NOT fix="Retest";' %column
	elif (re.match('[0-9]{4}_W[0-9]{1,2}',timeFrame)):
		query = 'SELECT %s FROM trackerData WHERE week=? AND resultType in ("PASS") AND NOT fix="Retest";' %column
	elif (re.match('[0-9]{4}_M[0-9]{1,2}',timeFrame)) and (column in ['orgFailNoNum']):
		query = 'SELECT %s,pitaTest,fix FROM trackerData WHERE month=? AND resultType in ("PASS") AND NOT fix="Retest";' %column
	elif (re.match('[0-9]{4}_M[0-9]{1,2}',timeFrame)):
		query = 'SELECT %s FROM trackerData WHERE month=? AND resultType in ("PASS") AND NOT fix="Retest";' %column

	to_filter = [timeFrame]
	conn = sqlite3.connect(dataBase_techTracker)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	fullResults = cur.execute(query, to_filter).fetchall()
	colWanted = []
	tests = collections.defaultdict(list)
	failTestFix = collections.defaultdict(list)
	listOfTests = []
	for item in fullResults:
		if item[column] not in ['NO MATCHING FAILURE FOUND IN PITA']:
			colWanted.append(item[column])
			if column in ['orgFailNoNum']:
				tests[item[column]].append(item['pitaTest'])
				if failTestFix[item[column]]:
					failTestFix[item[column]][item['pitaTest']].append(item['fix'])
				else:
					failTestFix[item[column]] = collections.defaultdict(list)
					failTestFix[item[column]][item['pitaTest']].append(item['fix'])
				listOfTests.append(item['pitaTest'])
	colWantedCount= collections.Counter(colWanted)
	if column in ['orgFailNoNum']:
		listOfTests = list(set(listOfTests))
		finalTestDict = collections.defaultdict(list)
		for item in listOfTests:
			finalTestDict[item] = [[],[]]
	counts=[]
	labels=[]
	xlabels = []
	
	for item in colWantedCount.most_common(5):
		labels.append(item[0])
		counts.append(item[1])
		xlabels.append({'text' : item[0][:25]})
		if column in ['orgFailNoNum']:
			testCount= collections.Counter(tests[item[0]])
			for test in testCount:
				finalTestDict[test][0].append(testCount[test])
				tmpfixlist = ','.join(failTestFix[item[0]][test])
				tmpfixlist = list(set(tmpfixlist.split(',')))
				tmpfixlist = ','.join(tmpfixlist)
				finalTestDict[test][1].append(tmpfixlist)
			for test in listOfTests:
				if test not in testCount:
					finalTestDict[test][0].append(0)
					finalTestDict[test][1].append("")

	if column in ['orgFailNoNum']:
		return labels,counts,xlabels,finalTestDict 
	else:
		return labels,counts,xlabels	

def copytree(src, dst, symlinks=False, ignore=None):
	print "Here we go"
	for item in os.listdir(src):
		
		s = os.path.join(src, item)
		d = os.path.join(dst, item)
		if os.path.isdir(s):
			print "%s is a directory" %item
			shutil.copytree(s, d, symlinks, ignore)
		else:
			print "%s is a file" %item
			shutil.copy2(s, d)
####################################################
##						ROUTES
####################################################

@app.route('/', methods=['GET'])										## Placeholder for 'home'
def home():
	return '''<h1>Distant Reading Archive</h1>
<p>A prototype API for distant reading of science fiction novels.</p>'''

@app.route('/api/v1/resources/docs', methods=['GET'])					## Show endpoints
def api_doc():
	file = open(apiDoc, 'rb') 
	text = file.read()
	file.close()
	return(text)	

@app.route('/api/v1/resources/getDB', methods=['GET'])					## Refresh Databases from Google Drive
def api_getDB():
	print "Triggering DB copy"
	try:
		dir = os.path.dirname(__file__)
		
		if not dir:
			dir = os.getcwd()
		print dir
		techDataDB_cloud = 'G:/My Drive/DBs/techTracker/techTracker.db'
		totalsDB_cloud = 'G:/My Drive/DBs/techTracker/techTracker_totals.db'
		totalsJson_cloud = 'G:/My Drive/DBs/techTracker/data'
		techDataDB_local = os.path.join(dir,'techTracker.db')
		totalsDB_local = os.path.join(dir,'techTracker_totals.db')
		totalsJson_local = os.path.join(dir,'data')

		print "Refreshing databases from Cloud"
		shutil.copy2(techDataDB_cloud,techDataDB_local)
		shutil.copy2(totalsDB_cloud,totalsDB_local)
		print totalsJson_cloud
		print totalsJson_local
		copytree(totalsJson_cloud,totalsJson_local)
		print "done"
	except:
		tb = sys.exc_info()[2]
 		tbinfo = traceback.format_tb(tb)[0]
 		# # Concatenate information together concerning the error into a message string
		pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
		# Capture the SNs where this function failed
 		print '%s \n' %(pymsg)
		return "Error"
	return "Done"

@app.route('/api/v1/resources/sgd', methods=['GET'])					## Show all entries for Units
def api_all_sgd():
	conn = sqlite3.connect(dataBase_SGD_Debub)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	all_entries= cur.execute('SELECT * FROM currentData;').fetchall()
	all_entries.insert(0,{'Number of Results' : len(all_entries)})		# Add Number of results as the first entry
	return jsonify(all_entries)


@app.route('/api/v1/resources/all', methods=['GET'])					## Show all entries for Units
def api_all():
	conn = sqlite3.connect(dataBase_techTracker)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	all_entries= cur.execute('SELECT * FROM trackerData;').fetchall()
	all_entries.insert(0,{'Number of Results' : len(all_entries)})		# Add Number of results as the first entry
	return jsonify(all_entries)
	
@app.route('/api/v1/resources/fails/all', methods=['GET'])				## Show all entries for Failed Units
def api_fails_all():
	conn = sqlite3.connect(dataBase_techTracker)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	all_fails = cur.execute('SELECT * FROM trackerData WHERE resultType=\'%s\';' %('FAIL')).fetchall()
	return jsonify(all_fails)

@app.route('/api/v1/resources/passes/all', methods=['GET'])				## Show all entries for Passed Units
def api_passes_all():
	conn = sqlite3.connect(dataBase_techTracker)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	all_passes = cur.execute('SELECT * FROM trackerData WHERE resultType=\'%s\';' %('PASS')).fetchall()
	return jsonify(all_passes)

@app.route('/api/v1/resources/count', methods=['GET'])					## Show Number of entries in techTracker.db
def api_count_all():
	conn = sqlite3.connect(dataBase_techTracker)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	all_passes = cur.execute('SELECT COUNT(serial) FROM trackerData').fetchall()
	return jsonify(all_passes)

@app.route('/api/v1/resources/dups', methods=['GET'])
def api_get_dups():
	conn = sqlite3.connect(dataBase_techTracker)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	all_passes = cur.execute('SELECT serial, dgqcKey FROM trackerData GROUP BY dgqcKey HAVING count(*) > 1;').fetchall()
	return jsonify(all_passes)

@app.route('/api/v1/resources/totals', methods=['GET'])
def api_totals_all():
	query = 'SELECT * FROM totals;'
	to_filter = []
	conn = sqlite3.connect(dataBase_totals)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	results = cur.execute(query, to_filter).fetchall()
	return jsonify(results)

@app.route('/api/v1/resources/test', methods=['GET'])
def api_test():
	query = "SELECT * from trackerData WHERE failText=?"
	to_filter = ["NO MATCHING FAILURE FOUND IN PITA"]
	conn = sqlite3.connect(dataBase_techTracker)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	fullResults = cur.execute(query, to_filter).fetchall()
	fullResults.insert(0,{'Number of Results' : len(fullResults)})
	return jsonify(fullResults)

@app.route('/api/v1/resources/test2', methods=['GET'])
def api_test2():
	query = "SELECT * from trackerData WHERE id in (select id from trackerData group by failTime, serial having count(*) >1);" # group by serial having count(*) >1;"
	to_filter = []
	conn = sqlite3.connect(dataBase_techTracker)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	fullResults = cur.execute(query, to_filter).fetchall()
	fullResults.insert(0,{'Number of Results' : len(fullResults)})
	return jsonify(fullResults)

@app.route('/api/v1/resources/user', methods=['GET'])
def api_user():
	query_parameters	= request.args
	user 				= query_parameters.get('user')
	week				= query_parameters.get('week')
	quarter				= query_parameters.get('quarter')
	year				= query_parameters.get('year')
	units				= query_parameters.get('units')
	
	query_base = "SELECT COUNT(serial) FROM trackerData WHERE"
	to_filter_all	= []
	to_filter_pass 	= []
	to_filter_fail 	= []
	to_filter_test 	= []
	to_filter_bgar 	= []
	to_filter_dbug 	= []
	to_filter_units	= []
	
	if (week and year and units):
		weekNum = year + "_W" + week
		query_units = 'SELECT * FROM trackerData WHERE week=? AND user=? ORDER BY dpfvTime ASC;'
		to_filter_units.extend([weekNum,user])
		conn = sqlite3.connect(dataBase_techTracker)
		conn.row_factory = dict_factory
		cur = conn.cursor()
		results = cur.execute(query_units, to_filter_units).fetchall()
		results.insert(0,{'Number of Results' : len(results)})		# Add Number of results as the first entry
		return jsonify(results)
		
	
	if (week and year):
		weekNum = year + "_W" + week
		query_all = query_base + ' week=? AND user=?'
		to_filter_all.extend([weekNum,user])
		query_user = query_base + ' week=? AND user=? AND resultType=?'
		
		to_filter_pass.extend([weekNum,user,"PASS"])
		to_filter_fail.extend([weekNum,user,"FAIL"])
		to_filter_dbug.extend([weekNum,user,"DBUG"])
		to_filter_test.extend([weekNum,user,"TEST"])
		to_filter_bgar.extend([weekNum,user,"BGAR"])
		conn = sqlite3.connect(dataBase_techTracker)
		conn.row_factory = dict_factory
		cur = conn.cursor()

		allResults	= cur.execute(query_all, to_filter_all).fetchall()
		passResults = cur.execute(query_user, to_filter_pass).fetchall()
		failResults = cur.execute(query_user, to_filter_fail).fetchall()
		dbugResults = cur.execute(query_user, to_filter_dbug).fetchall()
		testResults = cur.execute(query_user, to_filter_test).fetchall()
		bgarResults = cur.execute(query_user, to_filter_bgar).fetchall()
		
		allCount	= allResults[0]['COUNT(serial)']
		passCount 	= passResults[0]['COUNT(serial)']
		failCount 	= failResults[0]['COUNT(serial)']
		dbugCount 	= dbugResults[0]['COUNT(serial)']
		bgarCount 	= bgarResults[0]['COUNT(serial)']
		testCount 	= testResults[0]['COUNT(serial)']
		total 		= int(passCount) + int(failCount)
		if total > 0:
			percentPass = (int(passCount) / float(total)) * 100
		else: 
			percentPass = 0
			
		
	if (quarter and year):
		return("UNDER CONSTRUCTION")
	
	if (not (user and ((week and year) or (quarter and year)))):
		return page_not_found(404)

	return jsonify(Total=allCount,Pass=passCount,Fail=failCount, PercentPass=percentPass,Debug=dbugCount,BGAR=bgarCount,Testing=testCount)

@app.route('/api/v1/resources/filter', methods=['GET'])
@cross_origin()
def api_filter():
	query_parameters = request.args
	serial	=	query_parameters.get('serial')
	user = query_parameters.get('user')
	type = query_parameters.get('type')
	week	= query_parameters.get('week')
	year	= query_parameters.get('year')
	status	= query_parameters.get('status')
	quarter	= query_parameters.get('quarter')
	id		= query_parameters.get('id')
	delete 	= query_parameters.get('delete')
	get		= query_parameters.get('get')
	make	= query_parameters.get('make')

	query = "SELECT * FROM trackerData WHERE"
	to_filter = []
	var=''
	if make:
		if re.match('[0-9]{4}_Q\d',make):
			response = getIndivQuarter(make)
			return response
			
		if re.match('[0-9]{4}_W[0-9]{1,2}',make):
			response = getIndivQuarter(make)
			return response
			
		if re.match('[0-9]{4}_M[0-9]{1,2}',make):
			response = getIndivQuarter(make)
			return response
	if get:
		if get == 'main':
			quarter,year = getLastQuarter()
			lastQuarterString = "Q" + quarter + " " + year
			quarterPercentPass = getQuarterNumbers2(quarter,year)
			weekNum = getLastWeek()
			print "Weeknum: %s" %weekNum
			weekPercentPass = getWeekNumbers2(weekNum)
			overallPercentPass = getOverallNumbers('passPercent')
			mainHTML_file = 'C:/Apache24/htdocs/techTracker/main_response.html'
			with open(mainHTML_file,'rb') as f:
				mainResponse = f.read()
			mainResponse = mainResponse %(overallPercentPass,weekNum,weekPercentPass,lastQuarterString,quarterPercentPass)
			return mainResponse
		if get == 'quarterly':
			quarterlyChartResponse = getQuarterlyChart('quarter')
			return quarterlyChartResponse
			
		if get == 'weekly':
			weeklyChartResponse = getQuarterlyChart('week')
			return weeklyChartResponse
		
		if get == 'monthly':
			weeklyChartResponse = getQuarterlyChart('month')
			return weeklyChartResponse
		
		if re.match('[0-9]{4}_Q\d',get):
			with open('data/%s.json' %get) as data_file:    
				response = json.load(data_file)
			return jsonify(response)
			
		if re.match('[0-9]{4}_W[0-9]{1,2}',get):
			with open('data/%s.json' %get) as data_file:    
				response = json.load(data_file)
			return jsonify(response)
			
		if re.match('[0-9]{4}_M[0-9]{1,2}',get):
			with open('data/%s.json' %get) as data_file:    
				response = json.load(data_file)
			return jsonify(response)
	if id:
		query += ' id=? AND'
		to_filter.append(id)
	
	if delete:
		print delete
		query = "DELETE FROM trackerData WHERE"
		query += ' id=?'
		to_filter.append(delete)
		
		print query
		conn = sqlite3.connect(dataBase_techTracker)
		conn.row_factory = dict_factory
		cur = conn.cursor()

		cur.execute(query, to_filter)
		conn.commit()
		
		return("Done")
	if status and not (week or year):
		query += ' resultType=? AND'
		to_filter.append(status)
	if serial:
		query += ' serial=? AND'
		to_filter.append(serial)
	if user:
		query += ' user=? AND'
		to_filter.append(user)
	if type:
		if type.lower() == 'fail':
			query += ' resultType=? AND'
			var='FAIL'
		elif type.lower() == 'pass':
			query += ' resultType=? AND'
			var='PASS'
		elif type.lower() == 'dbug':
			query += ' resultType=? AND'
			var='DBUG'
		elif type.lower() == 'bgar':
			query += ' resultType=? AND'
			var='BGAR'
		if var:
			to_filter.append(var)
	if (week and year):
		
		weekNum = year + "_W" + week
		
		query += ' week=? AND'
		to_filter.append(weekNum)
		if status:
			passCount,failCount,percentPass,dbugCount,bgarCount,testCount = getWeekNumbers(weekNum,'all')
			return jsonify(Pass=passCount,Fail=failCount, PercentPass=percentPass,Debug=dbugCount,BGAR=bgarCount,Testing=testCount)
			
	if (quarter and year):
		if not status:
			pass
		else:
			passCount,failCount,percentPass,dbugCount,bgarCount,testCount = getQuarterNumbers(quarter,year,'all')

			return jsonify(Pass=passCount,Fail=failCount, PercentPass=percentPass,Debug=dbugCount,BGAR=bgarCount,Testing=testCount)

		return("UNDER CONSTRUCTION")
		
	if (not (user or delete or id or serial or type or status or get or ((week and year) or (quarter and year)))) or (type and not var):
		return page_not_found(404)

	query = query[:-4] + ';'
	print query
	conn = sqlite3.connect(dataBase_techTracker)
	conn.row_factory = dict_factory
	cur = conn.cursor()
	results = cur.execute(query, to_filter).fetchall()
	results.insert(0,{'Number of Results' : len(results)})		# Add Number of results as the first entry
	return jsonify(results)

####################################################
##				EL CUATRO CERO QUATRO
####################################################

@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404

####################################################
##						RUN
## DEFAULT PORT: 5000
####################################################
serve(TransLogger(app, format=format),host= '0.0.0.0', port=5000) 
