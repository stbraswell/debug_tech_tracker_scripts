This project supports the 'debug_tech_tracker_website'.  It contains the scripts, database and prepared files.
<h1>tech_tracker.py</h1>
This script is the main engine for acquiring the data.  It downloads a list of each debug technician name and employee number.  Using that info, it queries the Mfg. DB Api to get a
list of all unit serial numbers each tech. has worked on for the past week.  From there it determines:
1) What test the unit failed
2) What the failure text was.
3) If the tech. replaced any components on the board
4) If the unit has been retested

Once this info is acquired, this script stores it in a database, creates a summary of the info for each quarter, month and week.

<h1>tech_tracker_api.py</h1>
This script uses Flask to deploy an API that the 'debug_tech_tracker_website' uses to obtain the data to drive its pages.  It is also used to push the data and database to a backup server.