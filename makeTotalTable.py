import sqlite3
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String, DateTime, Float
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session
from sqlalchemy.ext.declarative import declarative_base
import datetime, json, requests
from dateutil import tz
import calendar
import collections
import socket

## THIS SCRIPT WAS CREATED TO UPDATE ALL ENTRIES IN THE TECHTRACKER.DB TRACKERDATA TABLE WITH QUARTER STRING
hostname = socket.gethostname()    
IPAddr = socket.gethostbyname(hostname) 
getViewAPI='http://' + IPAddr + ':5000/api/v1/resources/filter?make=%s'
print getViewAPI
from_zone = tz.gettz('UTC')
to_zone = tz.tzlocal()

quarters_dict = {'January':1,
				'February':1,
				'March':1,
				'April':2,
				'May':2,
				'June':2,
				'July':3,
				'August':3,
				'September':3,
				'October':4,
				'November':4,
				'December':4}




## CREATE THE DATABASE
#engine = create_engine('sqlite:///G:/My Drive/DBs/techTracker/techTracker.db')
engine = create_engine('sqlite:///techTracker_totals.db')
Base = declarative_base(bind=engine)

class Total(Base):
	__tablename__ 	= 'totals'
	id 				= Column(Integer, primary_key=True)
	timeframe  		= Column(String(40),nullable=False)
	numPass 		= Column(Integer,nullable=False)
	numFail			= Column(Integer,nullable=False)
	accuracy		= Column(Float(),nullable=False)
	topTechs		= Column(String(200),nullable=False)

	def __init__(self, timeframe, numPass, numFail, accuracy,topTechs): #, 
		self.timeframe 		= timeframe
		self.numPass 		= numPass
		self.numFail 		= numFail
		self.accuracy		= accuracy
		self.topTechs		= topTechs

class Fix(Base):
	__tablename__ 	= 'topFixes'
	id 				= Column(Integer, primary_key=True)
	timeframe  		= Column(String(40),nullable=False)
	testName		= Column(String(40),nullable=False)
	productName		= Column(String(40),nullable=False)
	failText 		= Column(String(500),nullable=False)
	fixes			= Column(String(200),nullable=False)
	numberFixed		= Column(Integer, primary_key=True)

	def __init__(self, timeframe, testName, productName, failText,fixes,numberFixed):
		self.timeframe 		= timeframe
		self.testName 		= testName
		self.productName 	= productName
		self.failText		= failText
		self.fixes			= fixes
		self.numberFixed	= numberFixed
		
		
Base.metadata.create_all()
dbSession = sessionmaker(bind=engine)
dbs = dbSession()
#engine_data = create_engine('sqlite:///G:/My Drive/DBs/techTracker/techTracker.db')
engine_data = create_engine('sqlite:///techTracker.db')
Base_data = declarative_base(bind=engine_data)
class Unit(Base):
	__tablename__ 	= 'trackerData'
	id 				= Column(Integer, primary_key=True)
	serial  		= Column(String(40),nullable=False)
	mesTest 		= Column(String(40),nullable=False)
	pitaTest		= Column(String(40),nullable=False)
	failKey			= Column(String(10),nullable=True)
	failTime		= Column(String(30),nullable=True)
	failText 		= Column(String(1000),nullable=True)
	user			= Column(String(20), nullable=False)
	dpfvKey			= Column(String(10),nullable=False)
	dpfvTime		= Column(String(30), nullable=False)
	# dpfvTime_date	= Column(Date, nullable=False, default=convertTime(dpfvTime))
	partNumber		= Column(String(60), nullable=False)
	fix				= Column(String(60),nullable=True)
	bgarKey			= Column(String(10),nullable=True)
	bgarTime 		= Column(String(30), nullable=True)
	dgqcKey			= Column(String(10),nullable=True)
	dgqcTime 		= Column(String(30), nullable=True)
	passFailKey		= Column(String(10),nullable=True)
	passFailTime 	= Column(String(30), nullable=True)
	postFailMesTest	= Column(String(40),nullable=False)
	postFailPitaTest= Column(String(40),nullable=True)
	postFailText	= Column(String(1000),nullable=True)
	resultType	 	= Column(String(5), nullable=True)
	comment 		= Column(String(100), nullable=True)
	deviations		= Column(String(30), nullable=True)
	week			= Column(String(10), nullable=False)
	quarter			= Column(String(10), nullable=True)
	orgFailNoNum	= Column(String(1000),nullable=True)
	month			= Column(String(10), nullable=True)
	def __init__(self, serial, mesTest, pitaTest, failKey, failTime, failText, user, dpfvKey, dpfvTime, partNumber, postFailMesTest, week, fix=None,
					bgarKey=None, bgarTime=None, dgqcKey=None, dgqcTime=None, passFailKey=None, 
					passFailTime=None,postFailPitaTest=None ,postFailText=None, resultType=None, comment=None, deviations = None, quarter=None, orgFailNoNum=None,
					month=None):
		self.serial 		= serial
		self.mesTest 		= mesTest
		self.pitaTest 		= pitaTest
		self.failKey		= failKey
		self.failTime		= failTime
		self.failText 		= failText
		self.fix 			= fix
		self.user 			= user
		self.dpfvKey 		= dpfvKey
		self.dpfvTime 		= dpfvTime
		# self.dpfvTime_date	= dpfvTime_date
		self.partNumber		= partNumber
		self.postFailMesTest= postFailMesTest
		self.week			= week
		self.bgarKey 		= bgarKey
		self.bgarTime 		= bgarTime
		self.dgqcKey 		= dgqcKey
		self.dgqcTime 		= dgqcTime
		self.passFailKey	= passFailKey
		self.passFailTime 	= passFailTime
		self.postFailPitaTest=	postFailPitaTest
		self.postFailText	= postFailText
		self.resultType		= resultType
		self.comment 		= comment
		self.deviations		= deviations
		self.quarter		= quarter
		self.orgFailNoNum	= orgFailNoNum
		self.month			= month
		
Base_data.metadata.create_all()
dbSession_data = sessionmaker(bind=engine_data)
dbs_data = dbSession_data()
#dbs =scoped_session(dbSession)
quarterUserAcc= {} #collections.defaultdict(list)

def getquarters():
	quarters = dbs_data.query(Unit.quarter).distinct().all()
	print "Number of Quarters: %d" %len(quarters)
	print quarters
	type='PASS'
	for quarter in quarters:
		myTops = ''
		quarterUserAcc= {} #collections.defaultdict(list)
		quarterUserAccList= {}
		users = dbs_data.query(Unit.user).filter(Unit.quarter == quarter[0], Unit.resultType == "PASS", Unit.fix != 'Retest').distinct().all()

		for userName in users:
			userPass = dbs_data.query(Unit).filter(Unit.quarter == quarter[0], Unit.resultType == "PASS", Unit.user==userName[0], Unit.fix != 'Retest').count()
			userFails = dbs_data.query(Unit).filter(Unit.quarter == quarter[0], Unit.resultType == "FAIL", Unit.user==userName[0], Unit.fix != 'Retest').count()
			# print "Number of FAILS for %s: %d" %(userName[0],userFails)
			userAccuracy = (userPass / float(userPass + userFails)) * 100
			userAccuracy = round(userAccuracy,2)
			quarterUserAccList[userName[0]]=[userPass,userFails]
			quarterUserAcc[userName[0]] = userAccuracy
			d = collections.Counter(quarterUserAcc)
		numberOfPasses = dbs_data.query(Unit).filter(Unit.quarter == quarter[0], Unit.resultType == "PASS", Unit.fix != 'Retest').count()
		numberOfFails = dbs_data.query(Unit).filter(Unit.quarter == quarter[0], Unit.resultType == "FAIL", Unit.fix != 'Retest').count()

		accuracy = (numberOfPasses / float(numberOfPasses + numberOfFails)) * 100
		accuracy = round(accuracy,2)

		for item in d.most_common(3):
			myTops = myTops + item[0] + ": " + str(item[1]) + "%% (Pass: %d) | (Fail: %d)\n" %(quarterUserAccList[item[0]][0],quarterUserAccList[item[0]][1])
		print "TOPS: %s" %myTops.strip()
		changed=0
		if len(dbs.query(Total).filter(Total.timeframe == quarter[0]).all()) > 0:
			existingUnit = dbs.query(Total).filter(Total.timeframe == quarter[0]).first()
			if existingUnit.numPass != numberOfPasses:
				existingUnit.numPass 		= numberOfPasses
				changed = 1
			if existingUnit.numFail != numberOfFails:
				existingUnit.numFail 		= numberOfFails
				changed = 1
			if existingUnit.accuracy != accuracy:
				existingUnit.accuracy		= accuracy
				changed = 1
			if existingUnit.topTechs != myTops:
				existingUnit.topTechs		= myTops
				changed = 1
		else:
			total = Total(quarter[0],numberOfPasses,numberOfFails,accuracy,myTops)
			dbs.add(total)
			changed = 1
		if changed:	
			dbs.commit()
		print "Getting json for %s" %quarter
		getViewJson(quarter)

def getWeeks():
	weeks = dbs_data.query(Unit.week).distinct().all()
	print "Number of Weeks: %d" %len(weeks)
	print weeks
	for week in weeks:
		myTops = ''
		weekUserAcc= {} #collections.defaultdict(list)
		weekUserAccList= {}
	
		users = dbs_data.query(Unit.user).filter(Unit.week == week[0], Unit.resultType == "PASS", Unit.fix != 'Retest').distinct().all()

		for userName in users:
			userPass = dbs_data.query(Unit).filter(Unit.week == week[0], Unit.resultType == "PASS", Unit.user==userName[0], Unit.fix != 'Retest').count()
			userFails = dbs_data.query(Unit).filter(Unit.week == week[0], Unit.resultType == "FAIL", Unit.user==userName[0], Unit.fix != 'Retest').count()
		
			userAccuracy = (userPass / float(userPass + userFails)) * 100
			userAccuracy = round(userAccuracy,2)
			weekUserAccList[userName[0]]=[userPass,userFails]
			weekUserAcc[userName[0]] = userAccuracy
			d = collections.Counter(weekUserAcc)

		numberOfPasses = dbs_data.query(Unit).filter(Unit.week == week[0], Unit.resultType == "PASS", Unit.fix != 'Retest').count()

		numberOfFails = dbs_data.query(Unit).filter(Unit.week == week[0], Unit.resultType == "FAIL", Unit.fix != 'Retest').count()
		accuracy = (numberOfPasses / float(numberOfPasses + numberOfFails)) * 100
		accuracy = round(accuracy,2)

		for item in d.most_common(3):
			myTops = myTops + item[0] + ": " + str(item[1]) + "%% (Pass: %d) | (Fail: %d)\n" %(weekUserAccList[item[0]][0],weekUserAccList[item[0]][1])
		print "TOPS: %s" %myTops.strip()
		changed=0
		if len(dbs.query(Total).filter(Total.timeframe == week[0]).all()) > 0:
			existingUnit = dbs.query(Total).filter(Total.timeframe == week[0]).first()
			if existingUnit.numPass != numberOfPasses:
				existingUnit.numPass 		= numberOfPasses
				changed = 1
			if existingUnit.numFail != numberOfFails:
				existingUnit.numFail 		= numberOfFails
				changed = 1
			if existingUnit.accuracy != accuracy:
				existingUnit.accuracy		= accuracy
				changed = 1
			if existingUnit.topTechs != myTops:
				existingUnit.topTechs		= myTops
				changed = 1
		else:
			total = Total(week[0],numberOfPasses,numberOfFails,accuracy,myTops)
			dbs.add(total)
			changed = 1
		if changed:	
			dbs.commit()
		print "Getting json for %s" %week
		getViewJson(week)

def getMonths():
	months = dbs_data.query(Unit.month).distinct().all()
	print "Number of months: %d" %len(months)
	print months
	for month in months:
		myTops = ''
		weekUserAcc= {} #collections.defaultdict(list)
		weekUserAccList= {}
	
		users = dbs_data.query(Unit.user).filter(Unit.month == month[0], Unit.resultType == "PASS", Unit.fix != 'Retest').distinct().all()

		for userName in users:
			userPass = dbs_data.query(Unit).filter(Unit.month == month[0], Unit.resultType == "PASS", Unit.user==userName[0], Unit.fix != 'Retest').count()
			userFails = dbs_data.query(Unit).filter(Unit.month == month[0], Unit.resultType == "FAIL", Unit.user==userName[0], Unit.fix != 'Retest').count()
		
			userAccuracy = (userPass / float(userPass + userFails)) * 100
			userAccuracy = round(userAccuracy,2)
			weekUserAccList[userName[0]]=[userPass,userFails]
			weekUserAcc[userName[0]] = userAccuracy
			d = collections.Counter(weekUserAcc)

		numberOfPasses = dbs_data.query(Unit).filter(Unit.month == month[0], Unit.resultType == "PASS", Unit.fix != 'Retest').count()

		numberOfFails = dbs_data.query(Unit).filter(Unit.month == month[0], Unit.resultType == "FAIL", Unit.fix != 'Retest').count()
		accuracy = (numberOfPasses / float(numberOfPasses + numberOfFails)) * 100
		accuracy = round(accuracy,2)

		for item in d.most_common(3):
			myTops = myTops + item[0] + ": " + str(item[1]) + "%% (Pass: %d) | (Fail: %d)\n" %(weekUserAccList[item[0]][0],weekUserAccList[item[0]][1])
		print "TOPS: %s" %myTops.strip()
		changed=0
		if len(dbs.query(Total).filter(Total.timeframe == month[0]).all()) > 0:
			existingUnit = dbs.query(Total).filter(Total.timeframe == month[0]).first()
			if existingUnit.numPass != numberOfPasses:
				existingUnit.numPass 		= numberOfPasses
				changed = 1
			if existingUnit.numFail != numberOfFails:
				existingUnit.numFail 		= numberOfFails
				changed = 1
			if existingUnit.accuracy != accuracy:
				existingUnit.accuracy		= accuracy
				changed = 1
			if existingUnit.topTechs != myTops:
				existingUnit.topTechs		= myTops
				changed = 1
		else:
			total = Total(month[0],numberOfPasses,numberOfFails,accuracy,myTops)
			dbs.add(total)
			changed = 1
		if changed:	
			dbs.commit()
		print "Getting json for %s" %month
		getViewJson(month)

def getViewJson(timeFrame):
	data = requests.get(getViewAPI %timeFrame, timeout=120)
	json_data = data.json()
	with open('data/%s.json' %timeFrame, 'wb') as f:
		json.dump(json_data, f, indent=4)

getquarters()
getWeeks()
getMonths()
# getViewJson('2019_Q1')
print "Done Updating Totals Table"
# data = dbs.query(Total).all()
# for item in data:
	# print item.__dict__
